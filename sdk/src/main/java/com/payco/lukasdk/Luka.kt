/*
 * Copyright (c) 2021. Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 26/8/21 4:23 p. m.
 */

package com.payco.lukasdk

import android.content.Context
import androidx.activity.result.ActivityResultCaller
import com.payco.lukasdk.services.auth.LukaAuthCredentials

/**
 * Luka Payments object utils
 */
open class Luka {
    /**
     * Api object
     */
    internal lateinit var api: LukaApi

    /**
     * Creates a new [LukaApi] instance
     */
    open fun onCreateBuilder(context: Context): LukaApiBuilder {
        return LukaApiBuilder(context)
    }

    /**
     * Resolves the singleton instance of [LukaApi]
     */
    internal fun resolveApi(
        context: Context,
        credentials: LukaAuthCredentials,
        setup: ((LukaApiBuilder) -> Unit)? = null
    ): LukaApi {
        if (!::api.isInitialized) {
            val appContext = context.applicationContext

            val builder = onCreateBuilder(appContext)
            setup?.invoke(builder)

            api = builder.build().apply {
                authenticate(credentials)
            }

        }

        return api
    }

    companion object {
        internal const val API_URL = "https://lukaapi.payco.net.ve/api/v1/"
        internal const val TEST_API_URL = "https://bspaycoapi-qa.payco.net.ve/api/v1/"
//        internal const val TEST_API_URL = "https://197a-45-230-169-73.ngrok.io/api/v1/"
        internal const val DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss"

        /**
         * Keeps the singleton [LukaApi] instance
         */
        internal lateinit var sInstance: Luka

        /**
         * Initializes
         */
        @JvmStatic fun initialize(context: Context): Luka {
            if (!::sInstance.isInitialized) {
                val appContext = context.applicationContext

                if (appContext is LukaApp) {
                    return initialize(context, appContext.getLukaCredentials()) {
                        appContext.onLukaBuild(it)
                    }
                } else {
                    throw RuntimeException("Application must implement LukaApp interface")
                }
            }

            return sInstance
        }

        @JvmStatic fun initialize(context: Context, credentials: LukaAuthCredentials, setup: ((LukaApiBuilder) -> Unit)? = null): Luka {
            return initializer {
                Luka().apply {
                    resolveApi(context.applicationContext, credentials, setup)
                }
            }
        }

        private fun initializer(func: () -> Luka): Luka {
            if (!::sInstance.isInitialized) {
                sInstance = func.invoke()
            }

            return sInstance
        }

        @JvmStatic fun api(): LukaApi {
            if (!::sInstance.isInitialized) {
                throw RuntimeException("Luka is not initialized")
            }

            return sInstance.api
        }

        @JvmStatic fun setup(caller: ActivityResultCaller) {
            api().setup(caller)
        }
    }
}
/*
 * Copyright (c) 2021. Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 26/8/21 4:23 p. m.
 */

package com.payco.lukasdk

import android.content.Context
import androidx.lifecycle.LiveData
import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.payco.lukasdk.http.ApiResponseFactory
import com.payco.lukasdk.http.HttpMapAdapter
import com.payco.lukasdk.http.LukaHeaderInterceptor
import com.payco.lukasdk.lifecycle.LiveDataAdapter
import com.payco.lukasdk.utils.Logger
import com.payco.lukasdk.utils.Threads
import okhttp3.Cache
import okhttp3.Dispatcher
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import java.util.concurrent.ExecutorService
import java.util.concurrent.LinkedBlockingQueue
import java.util.concurrent.ThreadPoolExecutor
import java.util.concurrent.TimeUnit

open class LukaApiBuilder(context: Context) {
    private val mContext: Context = context.applicationContext
    private var mTestModeEnabled = false

    fun useTestingApi(testing: Boolean): LukaApiBuilder {
        mTestModeEnabled = testing
        return this
    }

    open fun build(): LukaApi {
        val gson = onBuildGson()
        val apiUrl = apiUrl()

        log.debug("building API for $apiUrl")

        return LukaApi(
            mContext = mContext,
            mGson = gson,
            lukaRetrofit = onBuildRetrofit(mContext, apiUrl, gson),
            testModeEnabled = mTestModeEnabled
        )
    }

    protected open fun onBuildGson(): Gson {
        val singleLiveData: LiveDataAdapter.Single = LiveDataAdapter.Single()

        return GsonBuilder()
            .registerTypeAdapter(LiveData::class.java, singleLiveData)
//            .registerTypeAdapter(Date::class.java, LukaDateTimeAdapter())
            .registerTypeAdapter(Map::class.java, HttpMapAdapter())
            .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
            .create()
    }

    protected open fun onBuildRetrofit(context: Context, apiUrl: String, gson: Gson): Retrofit {
        return Retrofit.Builder()
            .baseUrl(apiUrl)
            .client(onBuildOkHttpClient(context))
            .callbackExecutor(Threads.threadExecutor)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(ApiResponseFactory())
            .build()
    }

    protected open fun onBuildOkHttpClient(context: Context): OkHttpClient {
        val httpCacheDirectory = File(context.filesDir, "payco")
        val cacheSize = 8L * 1024 * 1024 // 8 MiB

        val builder = OkHttpClient.Builder()
            .connectTimeout(30, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)
            .writeTimeout(60, TimeUnit.SECONDS)
            .dispatcher(Dispatcher(onBuildThreadExecutor()))
            .cache(Cache(httpCacheDirectory, cacheSize))
            .addInterceptor(LukaHeaderInterceptor())

        if (mTestModeEnabled) {
            val logger = object : HttpLoggingInterceptor.Logger {
                override fun log(message: String) {
                    var segment = message
                    try {
                        if (segment.length <= 4000) {
                            log.debug(segment)
                            return
                        } else {
                            val v = segment.substring(0, 4000)
                            log.debug(v)
                        }
                        segment = segment.substring(4000)
                        log(segment)
                    } catch (e: OutOfMemoryError) {
                        log.debug("Out of memory. Can not display response")
                    }
                }
            }
            val interceptor = HttpLoggingInterceptor(logger)
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
            builder.addInterceptor(interceptor)
        }


        return builder.build()
    }

    protected open fun onBuildThreadExecutor(): ExecutorService {
        return ThreadPoolExecutor(
            1, 2,
            60L, TimeUnit.SECONDS,
            LinkedBlockingQueue()
        )
    }

    private fun apiUrl(): String {
        return if (mTestModeEnabled) {
            Luka.TEST_API_URL
        } else {
            Luka.API_URL
        }
    }

    companion object {
        private val log = Logger.getLogger(LukaApiBuilder::class)
    }
}
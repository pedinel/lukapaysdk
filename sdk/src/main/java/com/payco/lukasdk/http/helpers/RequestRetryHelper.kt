/*
 * Copyright (c) 2021. Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 6/9/21 3:17 p. m.
 */

package com.payco.lukasdk.http.helpers

import com.payco.lukasdk.http.ApiRequest
import com.payco.lukasdk.http.HttpListener
import com.payco.lukasdk.utils.TaskHandler
import retrofit2.Response
import java.lang.ref.SoftReference
import java.util.concurrent.TimeUnit

/**
 * Helper to retry a Request in a fixed interval of time.
 * This requires a failed response to work.
 */
class RequestRetryHelper<T>(
    /**
     * The request to retry
     */
    request: ApiRequest<T>,

    /**
     * The amount of time to wait
     */
    amount: Long,

    /**
     * The time unit
     */
    unit: TimeUnit
) : HttpListener<T> {

    /**
     * Request ref
     */
    val requestWrapper = SoftReference(request)

    /**
     * Time interval in milliseconds
     */
    val interval = unit.toMillis(amount)

    /**
     * Trigger
     */
    val taskHandler = TaskHandler {
        val requestRef = requestWrapper.get()

        if (requestRef != null) {
            run(requestRef)
        }
    }

    override fun onResponse(response: Response<T>) {
        if (!response.isSuccessful) {
            //Queue action after delay
            taskHandler.postDelayed(interval)
        }
    }

    private fun run(request: ApiRequest<T>) {
        request.launchIfFailed()
    }
}
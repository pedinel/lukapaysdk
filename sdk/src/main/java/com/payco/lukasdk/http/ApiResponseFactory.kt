/*
 * Copyright (c) 2018 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 19/11/18 11:08 AM
 */
package com.payco.lukasdk.http

import retrofit2.CallAdapter
import retrofit2.Retrofit
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type

/**
 * Factory to create ApiResponse instances from Retrofit
 */
internal class ApiResponseFactory : CallAdapter.Factory() {
    override fun get(
        returnType: Type,
        annotations: Array<Annotation>,
        retrofit: Retrofit
    ): CallAdapter<*, *>? {
        val rawType = getRawType(returnType)
        var result = returnType

        return if (ApiRequest::class.java.isAssignableFrom(rawType)) {
            var rootClass = rawType
            while (ApiRequest::class.java != rootClass) {
                result = rootClass.genericSuperclass!!
                rootClass = getRawType(result)
            }
            if (result is ParameterizedType) {
                val dataType = getParameterUpperBound(
                    0,
                    result
                )
                LukaCallAdapter(dataType, rawType)
            } else {
                LukaCallAdapter<Any>(Any::class.java, rawType)
            }
        } else {
            null
        }
    }
}
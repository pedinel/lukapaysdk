/*
 * Copyright (c) 2020 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 17/12/20 08:21 PM.
 */
package com.payco.lukasdk.http

import android.os.SystemClock
import androidx.annotation.CheckResult
import androidx.annotation.MainThread
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.payco.lukasdk.http.helpers.RequestRefreshHelper
import com.payco.lukasdk.http.helpers.RequestRetryHelper
import com.payco.lukasdk.lifecycle.LiveDatas
import com.payco.lukasdk.lifecycle.then
import com.payco.lukasdk.utils.Logger
import okhttp3.Headers
import retrofit2.Call
import retrofit2.Response
import java.io.Closeable
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * ApiRequest for HTTP Calls
 */
class ApiRequest<Type> protected constructor(delegatedCall: Call<Type>) : Closeable {
    private val internalResponse: MutableLiveData<ApiResponse<Type>>
    private val internalDelegatedResponse: LiveData<ApiResponse<Type>>
    private val statusFlag: ApiRequestStatus = ApiRequestStatus()
    private val mCallback: LukaApiResponseHandler<Type>

    private val interceptorList = LinkedList<DataInterceptor<Type>>()
    private val responseListenerList = LinkedList<ResponseListener<Type>>()
    private val headersListenerList = LinkedList<HeadersInterceptor>()
    private val httpListenerList = LinkedList<HttpListener<Type>>()
    val loading: LiveData<Boolean>
    val data: LiveData<Type>
    val response: LiveData<ApiResponse<Type>>

    @Volatile
    private var call: Call<Type>?

    @Volatile
    private var activeListener: ((ApiRequest<*>) -> Unit)? = null

    init {
        call = delegatedCall

        internalResponse = LiveDatas.create { onActiveObservers() }

        internalDelegatedResponse = internalResponse.then { apiResponse, next ->
            if (apiResponse.isSuccessful) {
                statusFlag.setReplied()
            }
            resetCall()
            next.value(apiResponse)
            if (apiResponse.isSuccessful && statusFlag.isOutdated) {
                launch()
            }
        }

        loading = internalDelegatedResponse.then { response, next ->
            val loading: Boolean = (response as LukaApiResponseWrapper).isLoading()
            next.value(loading)
        }

        data = internalDelegatedResponse.then { response, next ->
            next.value(response.data())
        }

        response = internalDelegatedResponse.then { response, next ->
            if (!(response as LukaApiResponseWrapper).isLoading()) {
                next.value(response)
            }
        }

        val interceptor: ((Type) -> Type) = { response ->
            var result = response

            if (result != null) {
                interceptorList.forEach {
                    result = it.onIntercept(result)
                }
            }

            result
        }

        val responseListener: (Type) -> Unit = { response ->
            responseListenerList.forEach {
                it.onResponse(response)
            }
        }

        val headersListener: (Headers) -> Unit = { headers ->
            headersListenerList.forEach {
                it.onIntercept(headers)
            }
        }

        val httpListener: (Response<Type>) -> Unit = { response ->
            httpListenerList.forEach {
                it.onResponse(response)
            }
        }

        mCallback = LukaApiResponseHandler(
            internalResponse,
            interceptor,
            responseListener,
            httpListener,
            headersListener
        )
    }

    companion object {
        private val log: Logger = Logger.getLogger(ApiRequest::class)
        private val SILENT_LISTENER: (ApiRequest<*>) -> Unit = { it.launchIfNull() }
    }

    /**
     * Applies a custom interceptor to make data conversions.
     *
     * @param interceptor the Interceptor
     */
    fun addDataInterceptor(interceptor: DataInterceptor<Type>): ApiRequest<Type> {
        interceptorList.add(interceptor)
        return this
    }

    fun removeDataInterceptor(interceptor: DataInterceptor<Type>): ApiRequest<Type> {
        interceptorList.remove(interceptor)
        return this
    }

    fun addResponseListener(listener: ResponseListener<Type>): ApiRequest<Type> {
        responseListenerList.add(listener)
        return this
    }

    fun removeResponseListener(listener: ResponseListener<Type>): ApiRequest<Type> {
        responseListenerList.remove(listener)
        return this
    }

    fun addHeadersInterceptor(listener: HeadersInterceptor): ApiRequest<Type> {
        headersListenerList.add(listener)
        return this
    }

    fun removeHeadersInterceptor(listener: HeadersInterceptor): ApiRequest<Type> {
        headersListenerList.remove(listener)
        return this
    }

    fun addHttpListener(listener: HttpListener<Type>): ApiRequest<Type> {
        httpListenerList.add(listener)
        return this
    }

    fun removeHttpListener(listener: HttpListener<Type>): ApiRequest<Type> {
        httpListenerList.remove(listener)
        return this
    }

    fun withAutoRefresh(timeAmount: Long, unit: TimeUnit = TimeUnit.MILLISECONDS): ApiRequest<Type> {
        return addHttpListener(RequestRefreshHelper(this, timeAmount, unit))
    }

    fun withAutoRetry(timeAmount: Long, unit: TimeUnit = TimeUnit.MILLISECONDS): ApiRequest<Type> {
        return addHttpListener(RequestRetryHelper(this, timeAmount, unit))
    }

    /**
     * This makes the Request to execute automatically if needed
     *
     * @return self
     */
    @CheckResult
    fun withAutoLaunch(): ApiRequest<Type> {
        if (activeListener !== SILENT_LISTENER) {
            activeListener = SILENT_LISTENER
        }
        return this
    }

    @CheckResult
    fun withoutAutoLaunch(): ApiRequest<Type> {
        if (activeListener != null) {
            activeListener = null
        }
        return this
    }

    /**
     * This makes the Request to execute automatically if needed
     *
     * @return self
     */
    @CheckResult
    fun withAutoLaunch(timeAmount: Long, unit: TimeUnit = TimeUnit.MILLISECONDS): ApiRequest<Type> {
        activeListener = {
            if (it.hasData()) {
                launchIfElapsedMillis(unit.toMillis(timeAmount))
            } else {
                it.launch()
            }
        }
        return this
    }

    /**
     * Checks if the request is prepared to be launched safety
     */
    val isPrepared: Boolean
        get() {
            val call: Call<Type>?
            synchronized(mCallback) { call = this.call }
            return if (call == null) {
                false
            } else !call.isExecuted && !call.isCanceled
        }

    /**
     * Retrieves the Last Update Time using [SystemClock.elapsedRealtime]
     *
     * @return the Last Update Time
     */
    val lastUpdateTime: Long
        get() {
            val response: ApiResponse<Type> = mCallback.response() ?: return 0L
            return response.updateTime
        }

    val millisSinceLastUpdate: Long
        get() = SystemClock.elapsedRealtime() - lastUpdateTime

    /**
     * Launches the Request if not launched already. If any launched request is already completed,
     * this will work again
     */
    fun launch(): Boolean {
        return launchCall()
    }

    /**
     * Launches the Request if data is null.
     */
    fun launchIfNull(): Boolean {
        return if (!hasData()) {
            launch()
        } else {
            false
        }
    }

    /**
     * Launches the Request if data is null.
     */
    fun launchIfFailed(): Boolean {
        val response: ApiResponse<Type> = mCallback.response() ?: return false
        return response.isSuccessful || launch()
    }

    fun launchIfElapsedMillis(millis: Long): Boolean {
        return if (millisSinceLastUpdate >= millis) {
            launch()
        } else {
            false
        }
    }

    fun launchAndWait(): ApiResponse<Type>? {
        return launchCallSync()
    }

    @CheckResult
    fun hasData(): Boolean {
        val response: ApiResponse<Type>? = mCallback.response()
        return response?.data() != null
    }

    @get:CheckResult
    val responseData: Type?
        get() {
            val response: ApiResponse<Type>? = mCallback.response()
            return response?.data()
        }

    /**
     * Cancels the Request
     */
    @MainThread
    fun cancel() {
        val call: Call<Type>?
        synchronized(mCallback) { call = this.call }
        if (call == null) return
        if (!call.isCanceled) {
            call.cancel()
            mCallback.finishLoad()
        }
    }

    /**
     * Forces data replacement without consuming the request. This only works if the last request is successful
     * @param data the new data to replace
     */
    fun replaceData(data: Type) {
        mCallback.replaceData(data)
    }

    fun replaceResponse(data: Type) {
        mCallback.onResponse(call, Response.success(data))
    }

    val isNotLaunched: Boolean
        get() = internalResponse.value == null

    fun observe(owner: LifecycleOwner, observer: Observer<ApiResponse<Type>>) {
        response.observe(owner, observer)
    }

    @MainThread
    fun removeObservers(owner: LifecycleOwner) {
        response.removeObservers(owner)
        data.removeObservers(owner)
        loading.removeObservers(owner)
    }

    fun reset() {
        synchronized(mCallback) {
            val call = call
            if (call != null && !call.isCanceled && call.isExecuted) {
                this.call = call.clone()
                call.cancel()
                mCallback.finishLoad()
            }
        }
    }

    /**
     * Marks the request as outdated. The request will be updated the next time is needed
     */
    fun setOutdated() {
        if (statusFlag.setOutdated()) {
            launch()
        }
    }

    /**
     * Marks the request as outdated. The request will be updated the next time is needed
     */
    fun setOutdatedIfElapsedMillis(millis: Long): Boolean {
        if (millisSinceLastUpdate >= millis) {
            setOutdated()
            return true
        } else {
            return false
        }
    }

    /**
     * Allow data conversions or transformations for the request. If null, no response were retrieved.
     * This is only triggered in a successful response.
     *
     * @param data the original response
     * @return the output. This can be passed to a custom interceptor. see [.setDataInterceptor]
     */
    protected fun onDataResponse(data: Type?): Type? {
        return data
    }

    /**
     * The Request has observers
     */
    @MainThread
    protected fun onActiveObservers() {
        if (statusFlag.isOutdated) {
            launch()
        }
        activeListener?.invoke(this)
    }

    private fun resetCall() {
        val call: Call<Type>?
        synchronized(mCallback) { call = this.call }
        if (call == null) return
        if (!call.isCanceled && call.isExecuted) {
            val copy = call.clone()
            synchronized(mCallback) {
                if (this.call != null) {
                    this.call = copy
                }
            }
        }
    }

    @Synchronized
    private fun prepareNewLaunch() {
        val call: Call<Type>?
        synchronized(mCallback) { call = this.call }
        if (call == null) return
        if (call.isCanceled || call.isExecuted) {
            val copy = call.clone()
            synchronized(mCallback) {
                if (this.call != null) {
                    this.call = copy
                }
            }
        }
    }

    @Synchronized
    private fun launchCall(): Boolean {
        if (isPrepared) {
            statusFlag.setLaunched()
            mCallback.execute(call!!, true)
            return true
        }
        return false
    }

    @Synchronized
    private fun launchCallSync(): ApiResponse<Type>? {
        prepareNewLaunch()
        mCallback.execute(call!!, false)
        return mCallback.response()
    }

    @MainThread
    override fun close() {
        val call: Call<Type>?
        synchronized(mCallback) {
            call = this.call

            if (call == null) {
                return@close
            }

            this.call = null
        }

        call!!.cancel()
        mCallback.finishLoad()
    }

    @MainThread
    fun close(owner: LifecycleOwner) {
        close()
        removeObservers(owner)
    }

    override fun toString(): String {
        val call = call ?: return super.toString()
        return String.format("ApiRequest %s", call.request().url)
    }
}
package com.payco.lukasdk.http

import com.payco.lukasdk.utils.Logger
import retrofit2.Retrofit
import java.lang.ref.SoftReference
import kotlin.reflect.KClass

/**
 * Creates and provides Http Services
 */
open class HttpServiceProvider(
    private val retrofit: Retrofit,
) {
    private val services: HashMap<KClass<*>, SoftReference<*>> = HashMap()

    open fun <T : Any> getService(clazz: KClass<T>): T {
        val holder: SoftReference<*>? = services[clazz]
        val cached = holder?.get()

        if (cached != null) {
            return cached as T
        }

        val ref = retrofit.create(clazz.java)
        services[clazz] = SoftReference(ref)
        log.debug("New Service: %s", clazz.simpleName ?: "Anonymous")
        return ref
    }

    companion object {
        private val log = Logger.getLogger(HttpServiceProvider::class)
    }
}
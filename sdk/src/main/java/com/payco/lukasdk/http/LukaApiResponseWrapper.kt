/*
 * Copyright (c) 2019 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 31/05/19 11:39 AM
 */
package com.payco.lukasdk.http

import android.os.SystemClock
import com.google.gson.JsonIOException
import com.google.gson.JsonSyntaxException
import com.payco.lukasdk.Luka
import com.payco.lukasdk.utils.Logger
import com.payco.lukasdk.utils.Threads
import okhttp3.Headers
import retrofit2.Response
import java.io.IOException
import java.util.concurrent.atomic.AtomicBoolean
import java.util.concurrent.atomic.AtomicReference
import kotlin.reflect.KClass
import kotlin.reflect.cast

internal class LukaApiResponseWrapper<T> : ApiResponse<T> {
    private val mLoading = AtomicBoolean(false)
    private val mData = AtomicReference<T>()
    private val mResponse = AtomicReference<Response<T>?>()
    private val mError = AtomicReference<Throwable>()
    private val mErrorBody = AtomicReference<String?>()

    @Volatile
    private var mDataUpTime: Long

    @Volatile
    private var mUpdateTime: Long

    @Volatile
    private var mCachedError: Any? = null

    constructor() {
        mDataUpTime = 0L
        mUpdateTime = SystemClock.elapsedRealtime()
    }

    constructor(dataUpTime: Long) {
        this.mDataUpTime = dataUpTime
        mUpdateTime = SystemClock.elapsedRealtime()
    }

    fun isLoading(): Boolean {
        return mLoading.get()
    }

    override val isSuccessful: Boolean
        get() = !hasFailed() && mResponse.get()?.isSuccessful ?: false

    override fun hasFailed(): Boolean {
        return mError.get() != null
    }

    override fun code(): Int {
        val error = mError.get()
        val payload = mResponse.get()
        return if (error == null && payload != null) payload.code() else -1
    }

    override val isCacheResponse: Boolean
        get() = isSuccessful && mResponse.get()!!.raw().cacheResponse != null

    override fun hasError(): Boolean {
        return !hasFailed() && !(mResponse.get()?.isSuccessful ?: false)
    }

    override val updateTime: Long
        get() = mUpdateTime

    fun setLoading(): LukaApiResponseWrapper<T> {
        mLoading.set(true)
        return this
    }

    fun setNotLoading(): LukaApiResponseWrapper<T> {
        mLoading.set(false)
        return this
    }

    fun setResponse(
        r: Response<T>,
        interceptor: (T) -> T,
        l: (T) -> Unit
    ): LukaApiResponseWrapper<T> {
        mLoading.set(false)
        mResponse.set(r)
        mError.set(null)
        if (r.isSuccessful) {
            var result: T? = r.body()

            if (result != null) {
                try {
                    result = interceptor.invoke(result)
                } catch (e: Throwable) {
                    log.error(e, "Error intercepting request: %s", this)
                }
            }

            mData.set(result)
            mDataUpTime = SystemClock.elapsedRealtime()
            mUpdateTime = mDataUpTime

            if (result != null) {
                Threads.post { l.invoke(result) }
            }
        } else {
            mUpdateTime = mDataUpTime
            try {
                if (r.errorBody() == null) {
                    log.warn("Request failed %d: unknown", r.code())
                } else {
                    mErrorBody.set(r.errorBody()!!.string())
                    log.warn("Request failed %d: %s", r.code(), mErrorBody.get() ?: "null")
                }
            } catch (ignored: IOException) {
            }
        }
        return this
    }

    fun setData(
        rawResult: T,
        interceptor: (T) -> T,
        l: (T) -> Unit
    ): LukaApiResponseWrapper<T> {
        var result = rawResult
        result = interceptor.invoke(result)
        mData.set(result)
        val emit = result
        Threads.post { l.invoke(emit) }
        return this
    }

    fun setError(t: Throwable): LukaApiResponseWrapper<T> {
        mLoading.set(false)
        mError.set(t)
        mUpdateTime = mDataUpTime
        return this
    }

    override fun data(): T {
        return mData.get()
    }

    override fun headers(): Headers? {
        return mResponse.get()?.headers()
    }

    @Throws(JsonSyntaxException::class, JsonIOException::class)
    override fun <R : ApiErrorResponse> error(clazz: KClass<R>): R {
        val errorFromCache = mCachedError
        if (errorFromCache != null && clazz.isInstance(errorFromCache)) {
            return clazz.cast(errorFromCache)
        }

        if (clazz != ApiErrorResponse::class && mErrorBody.get() != null) {
            try {
                val out: R = Luka.api().mGson.fromJson(mErrorBody.get(), clazz.java)
                mCachedError = out
                return out
            } catch (e: Throwable) {
                log.error(e, "Unable to parse error as: %s. %s", clazz, mErrorBody.get() ?: "null")
            }

            //something went wrong

        }

        //native parsing

        val body = Luka.api().mGson.fromJson(mErrorBody.get(), Map::class.java) as Map<String, Any>
        val out = clazz.java.newInstance()

        out.initialize(code(), body)
        return out
    }

    override fun exception(): Throwable {
        return mError.get()
    }

    fun copy(): LukaApiResponseWrapper<T> {
        val copy = LukaApiResponseWrapper<T>(mDataUpTime)
        copy.mData.set(mData.get())
        return copy
    }

    private fun content(): Any? {
        return when {
            isLoading() -> {
                "Loading: true"
            }
            isSuccessful -> {
                data()
            }
            hasError() -> {
                mResponse.get()!!.errorBody()
            }
            else -> {
               exception().message
            }
        }
    }

    override fun toString(): String {
        return String.format("code: %s, data: %s", code(), content())
    }

    companion object {
        private val log: Logger = Logger.getLogger(LukaApiResponseWrapper::class)
    }
}
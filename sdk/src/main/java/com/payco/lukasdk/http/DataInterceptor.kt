/*
 * Copyright (c) 2021. Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 31/8/21 9:29 a. m.
 */

package com.payco.lukasdk.http

interface DataInterceptor<T> {
    /**
     * Intercepts the response to allow data handling.
     *
     * @param response the original response
     * @see ApiRequest.onDataResponse
     * @return the converted response
     */
    fun onIntercept(response: T): T
}
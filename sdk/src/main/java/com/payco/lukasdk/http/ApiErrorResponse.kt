/*
 * Copyright (c) 2018 Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 19/11/18 11:08 AM
 */
package com.payco.lukasdk.http

/**
 * Generic Api Error holder
 */
open class ApiErrorResponse {

    var responseCode: Int = -1
    var payload: Map<String, Any> = HashMap()

    internal fun initialize(code: Int, content: Map<String, Any>) {
        responseCode = code
        payload = content
    }
}
/*
 * Copyright (c) 2021. Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 31/8/21 9:30 a. m.
 */

package com.payco.lukasdk.http

import okhttp3.Headers

interface HeadersInterceptor {
    /**
     * Intercepts response headers
     */
    fun onIntercept(headers: Headers)
}
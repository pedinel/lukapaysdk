package com.payco.lukasdk.lifecycle

import androidx.annotation.MainThread
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.Observer
import com.payco.lukasdk.utils.Threads
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicInteger

/**
 * Function Aliases
 */

private typealias Procedure<T> = (T) -> Unit
private typealias Procedure2<A, B> = (A, B) -> Unit

private typealias Predicate<T> = (T) -> Boolean

private typealias Function1<A, R> = (A) -> R
private typealias Function2<A, B, R> = (A, B) -> R
private typealias Function3<A, B, C, R> = (A, B, C) -> R

data class Holder2<out T1, out T2>(
    val value1: T1,
    val value2: T2,
) {
    override fun toString(): String = "($value1, $value2)"
}

data class Holder3<out T1, out T2, out T3>(
    val value1: T1,
    val value2: T2,
    val value3: T3,
) {
    override fun toString(): String = "($value1, $value2, $value3)"
}

data class Holder4<out T1, out T2, out T3, out T4>(
    val value1: T1,
    val value2: T2,
    val value3: T3,
    val value4: T4,
) {
    override fun toString(): String = "($value1, $value2, $value3, $value4)"
}

data class Holder5<out T1, out T2, out T3, out T4, out T5>(
    val value1: T1,
    val value2: T2,
    val value3: T3,
    val value4: T4,
    val value5: T5,
) {
    override fun toString(): String = "($value1, $value2, $value3, $value4, $value5)"
}


/**
 * Raw Map function
 */
private fun <In, Out> LiveData<In>.internalMap(
    async: Boolean,
    function: Function1<In, Out>
): LinkedLiveData<Out> {
    val result: LinkedLiveData<Out> = LinkedLiveData()
    return result.addSourceForMap(this, async, function)
}

/**
 * Raw action performer
 */
private fun <In, Out> LiveData<In>.internalDo(
    async: Boolean,
    function: (In, Next<Out>) -> Unit
): LinkedLiveData<Out> {
    val result: LinkedLiveData<Out> = LinkedLiveData()
    return result.addSourceForOperation(this, async, function)
}

/**
 * Raw flatMap function
 */
private fun <In, Out> LiveData<In>.internalSwitchMap(
    function: Function1<In, LiveData<Out>>,
): LinkedLiveData<Out> {
    val result: LinkedLiveData<Out> = LinkedLiveData()
    return result.addSourceForSwitchMap(this, function)
}

/**
 * Applies a function to convert a LiveData's value into another
 */
fun <In, Out> LiveData<In>.map(
    function: Function1<In, Out>
): LinkedLiveData<Out> {
    return internalMap(false, function)
}

fun <Type1, Type2, Out> LiveData<Holder2<Type1, Type2>>.map(
    function: Function2<Type1, Type2, Out>
): LinkedLiveData<Out> {
    return internalMap(false) {
        function.invoke(it.value1, it.value2)
    }
}

fun <Type1, Type2, Type3, Out> LiveData<Holder3<Type1, Type2, Type3>>.map(
    function: Function3<Type1, Type2, Type3, Out>
): LinkedLiveData<Out> {
    return internalMap(false) {
        function.invoke(it.value1, it.value2, it.value3)
    }
}

/**
 * Applies a function to convert a LiveData value into a LiveData
 */
fun <In, Out> LiveData<In>.flatMap(
    function: Function1<In, LiveData<Out>>,
): LinkedLiveData<Out> {
    return internalSwitchMap(function)
}

fun <Type1, Type2, Out> LiveData<Holder2<Type1, Type2>>.flatMap(
    function: Function2<Type1, Type2, LiveData<Out>>,
): LinkedLiveData<Out> {
    return internalSwitchMap {
        function.invoke(it.value1, it.value2)
    }
}

fun <Type1, Type2, Type3, Out> LiveData<Holder3<Type1, Type2, Type3>>.flatMap(
    function: Function3<Type1, Type2, Type3, LiveData<Out>>,
): LinkedLiveData<Out> {
    return internalSwitchMap {
        function.invoke(it.value1, it.value2, it.value3)
    }
}

/**
 * Executes an action before next emission
 */
fun <Type> LiveData<Type>.doOnNext(
    function: Procedure<Type>,
): LinkedLiveData<Type> {
    return internalDo(false) { value, next ->
        function.invoke(value)
        next.value(value)
    }
}

/**
 * Executes an action and let user decide the further emission
 */
fun <In, Out> LiveData<In>.then(
    function: (In, Next<Out>) -> Unit,
): LinkedLiveData<Out> {
    return internalDo(false, function)
}

fun <Type1, Type2, Out> LiveData<Holder2<Type1, Type2>>.then(
    function: (Type1, Type2, Next<Out>) -> Unit,
): LinkedLiveData<Out> {
    return internalDo(false) { input, next ->
        function(input.value1, input.value2, next)
    }
}

/**
 * Emits only if a condition is meet
 */
fun <Type> LiveData<Type>.whenCondition(
    predicate: Predicate<Type>
): LinkedLiveData<Type> {
    return internalDo(false) {input, next ->
        if (predicate.invoke(input)) {
            next.value(input)
        }
    }
}

fun <Type> MediatorLiveData<Type>.single(): LinkedLiveData<Type> {
    val result = LinkedLiveData<Type>()

    result.addSourceForOperation(this, false) { input, next ->
        next.value(input)
        result.removeSource(this)
    }

    return result
}


/**
 * Waits for [amount] of time before emitting. If many values are received, they all will be emitted
 * immediately one before the another according to the delay specified.
 */
fun <Type> LiveData<Type>.delay(
    amount: Long,
    timeUnit: TimeUnit,
): LinkedLiveData<Type> {
    return internalDo(false) { value, next ->
        Threads.postDelayed(timeUnit.toMillis(amount)) {
            next.value(value)
        }
    }
}

/**
 * Waits for [amount] of time before emitting. If many values are received, they previous one will
 * be cancelled if it was not emmited yet. This is useful for triggering a search request when the
 * user types into an input field.
 */
fun <Type> LiveData<Type>.debounce(
    amount: Long,
    timeUnit: TimeUnit,
): LinkedLiveData<in Type> {
    var task: Threads.DelayedTask? = null

    return internalDo(false) { value, next ->
        task.let {
            if (it !== null) {
                it.cancel()
            }
        }

        task = Threads.delayedTask(timeUnit.toMillis(amount)) {
            next.value(value)
        }
    }
}

/**
 * Repeats the last emission.
 */
fun <Type> LiveData<Type>.repeat(
    times: Int,
): LinkedLiveData<in Type> {
    return internalDo(false) { value, next ->
        for (count in 0 until times) {
            next.value(value)
        }
    }
}

fun <Type> LiveData<Type>.interval(
    amount: Long,
    timeUnit: TimeUnit,
): LinkedLiveData<Type> {
    val result: IntervalLiveData<Type> = IntervalLiveData(timeUnit.toMillis(amount))
    result.addSource(this)

    return result
}

internal fun <Out> combineIfAll(
    async: Boolean,
    sources: Array<LiveData<out Any>>,
    function: Function1<Array<out Any?>, Out>
) : LinkedLiveData<Out> {

    val buffer = arrayOfNulls<Any>(sources.size)
    val count = AtomicInteger(sources.size)
    val out = LinkedLiveData<Out>()

    val publisher: TaskRunner = if (async) {
        TaskRunner {
            Threads.queueLatestAsync(out) {
                out.postValue(function.invoke(buffer))
            }
        }
    } else {
        TaskRunner {
            out.postValue(function.invoke(buffer))
        }
    }

    for (i in sources.indices) {
        buffer[i] = LinkedLiveData.EMPTY
    }

    for (i in sources.indices) {
        out.addSource(sources[i]) { s ->
            val left = if (buffer[i] === LinkedLiveData.EMPTY)
                count.decrementAndGet()
            else
                count.get()

            val emit = (buffer[i] != s)
            buffer[i] = s

            if (left == 0 && emit) {
                publisher.launch()
            }
        }
    }

    publisher.start()

    return out
}

internal fun <Out> combineIfAny(
    async: Boolean,
    sources: Array<LiveData<Any>>,
    function: Function1<Array<Any?>, Out>
) : LinkedLiveData<Out> {

    val buffer = arrayOfNulls<Any>(sources.size)
    val out = LinkedLiveData<Out>()

    val publisher: TaskRunner = if (async) {
        TaskRunner {
            Threads.queueLatestAsync(out) {
                out.postValue(function.invoke(buffer))
            }
        }
    } else {
        TaskRunner {
            out.postValue(function.invoke(buffer))
        }
    }

    for (i in sources.indices) {
        out.addSource(sources[i]) { s ->
            val emit = (buffer[i] != s)
            buffer[i] = s

            if (emit) {
                publisher.launch()
            }
        }
    }

    publisher.start()

    return out
}


fun <A, B, Out> LiveDatas.Companion.combine(
    a: LiveData<out A>,
    b: LiveData<out B>,
    function: Function2<A, B, Out>
) : LinkedLiveData<Out> {
    return combineIfAll(false, arrayOf(a, b)) {
        function.invoke(
            it[0] as A,
            it[1] as B,
        )
    }
}

fun <A, B, C, Out> LiveDatas.Companion.combine(
    a: LiveData<out A>,
    b: LiveData<out B>,
    c: LiveData<out C>,
    function: Function3<A, B, C, Out>
) : LinkedLiveData<Out> {
    return combineIfAll(false, arrayOf(a, b, c)) {
        function.invoke(
            it[0] as A,
            it[1] as B,
            it[2] as C,
        )
    }
}

//fun <A, B, C, D, Out> LiveDatas.Companion.combine(
//    a: LiveData<out A>,
//    b: LiveData<out B>,
//    c: LiveData<out C>,
//    d: LiveData<out D>,
//    function: Function4<A, B, C, D, Out>
//) : LinkedLiveData<Out> {
//    return combineIfAll(false, arrayOf(a, b, c, d)) {
//        function.invoke(
//            it[0] as A,
//            it[1] as B,
//            it[2] as C,
//            it[3] as D,
//        )
//    }
//}
//
//fun <A, B, C, D, E, Out> LiveDatas.Companion.combine(
//    a: LiveData<out A>,
//    b: LiveData<out B>,
//    c: LiveData<out C>,
//    d: LiveData<out D>,
//    e: LiveData<out E>,
//    function: Function5<A, B, C, D, E, Out>
//) : LinkedLiveData<Out> {
//    return combineIfAll(false, arrayOf(a, b, c, d, e)) {
//        function.invoke(
//            it[0] as A,
//            it[1] as B,
//            it[2] as C,
//            it[3] as D,
//            it[4] as E,
//        )
//    }
//}

fun <Type, A, Out> LiveData<Type>.combineWith(
    a: LiveData<out A>,
    function: Function2<Type, A, Out>
) : LinkedLiveData<Out> {
    return LiveDatas.combine(this, a, function)
}

fun <Type, Other> LiveData<Type>.combineWith(
    a: LiveData<Other>
) : LinkedLiveData<Holder2<Type, Other>> {
    return LiveDatas.combine(this, a) { value1, value2 ->
        Holder2(value1, value2)
    }
}

fun <Type, A, B, Out> LiveData<Type>.combineWith(
    a: LiveData<out A>,
    b: LiveData<out B>,
    function: Function3<Type, A, B, Out>
) : LinkedLiveData<Out> {
    return LiveDatas.combine(this, a, b, function)
}

fun <Type, Other1, Other2> LiveData<Type>.combineWith(
    a: LiveData<Other1>,
    b: LiveData<Other2>
) : LinkedLiveData<Holder3<Type, Other1, Other2>> {
    return LiveDatas.combine(this, a, b) { value1, value2, value3 ->
        Holder3(value1, value2, value3)
    }
}

fun <Type> LiveDatas.Companion.merge(
    a: LiveData<out Type>,
    b: LiveData<out Type>,
) : LinkedLiveData<Type> {
    val operation = { item: Type -> item }

    return LinkedLiveData<Type>().apply {
        addSourceForMap(a, false, operation)
        addSourceForMap(b, false, operation)
    }
}

fun <Type> LiveData<Type>.mergeWith(
    a: LiveData<out Type>,
) : LinkedLiveData<Type> {
    return LiveDatas.merge(this, a)
}


// Iterable processing

fun <ItemType, Type : Iterable<ItemType>> LiveData<Type>.filter(
    predicate: Predicate<ItemType>,
) : LinkedLiveData<in Type> {
    return internalMap(true) { list ->
        return@internalMap list.filter(predicate)
    }
}

fun <ItemType, Type : Iterable<ItemType>> LiveData<out Type>.first(): LinkedLiveData<in Type> {
    return internalMap(false) { list ->
        return@internalMap list.first()
    }
}


fun <ItemType, Type : Iterable<ItemType>> LiveData<Type>.last(): LinkedLiveData<in Type> {
    return internalMap(false) { list ->
        return@internalMap list.last()
    }
}

fun <RType, ItemType : RType, Type : Iterable<ItemType>> LiveData<Type>.reduce(
    operation: Function2<RType, ItemType, RType>,
) : LinkedLiveData<in RType> {
    return internalMap(true) { list ->
        return@internalMap list.reduce(operation)
    }
}

fun <RType, ItemType : RType, Type : Iterable<ItemType>> LiveData<Type>.scan(
    operation: Function2<RType, ItemType, RType>,
) : LinkedLiveData<in RType> {
    return internalDo(true) { list, next ->
        val iterator = list.iterator()
        if (!iterator.hasNext()) return@internalDo
        var accumulator: RType = iterator.next()
        next.value(accumulator)
        while (iterator.hasNext()) {
            accumulator = operation(accumulator, iterator.next())
            next.value(accumulator)
        }
    }
}

fun <ItemType, Type : Iterable<ItemType>> LiveData<Type>.all(
    predicate: Predicate<ItemType>,
) : LinkedLiveData<in Type> {
    return internalMap(true) { list ->
        return@internalMap list.all(predicate)
    }
}

fun <ItemType, Type : Iterable<ItemType>, RType> LiveData<Type>.mapEach(
    transform: Function1<ItemType, RType>,
) : LinkedLiveData<List<RType>> {
    return internalMap(true) { list ->
        return@internalMap list.map(transform)
    }
}

fun <ItemType, Type : Iterable<ItemType>> LiveData<Type>.contains(
    element: ItemType,
) : LinkedLiveData<Boolean> {
    return internalMap(true) { list ->
        return@internalMap list.contains(element)
    }
}

fun <ItemType, Type : Iterable<ItemType>> LiveData<Type>.count(
) : LinkedLiveData<Int> {
    return internalMap(true) { list ->
        return@internalMap list.count()
    }
}

fun <ItemType, Type : Iterable<ItemType>> LiveData<Type>.distinct(
) : LinkedLiveData<List<ItemType>> {
    return internalMap(true) { list ->
        return@internalMap list.distinct()
    }
}

fun <ItemType, Type : Iterable<ItemType>> LiveData<Type>.toList(
) : LinkedLiveData<List<ItemType>> {
    return internalMap(true) { list ->
        return@internalMap list.toList()
    }
}

fun <ItemType, Type : Iterable<ItemType>> LiveData<Type>.toSortedList(
    predicate: Function2<ItemType, ItemType, Int>,
) : LinkedLiveData<List<ItemType>> {
    return internalMap(true) { list ->
        return@internalMap list.sortedWith(predicate)
    }
}

@MainThread
fun <Type> LiveData<Type>.once(owner: LifecycleOwner, procedure: Procedure<Type>) {
    val observer = object : Observer<Type> {
        override fun onChanged(it: Type) {
            procedure.invoke(it)
            removeObserver(this)
        }
    }

    observe(owner, observer)
}

@MainThread
fun <Type> LiveData<Type>.once(procedure: Procedure<Type>) {
    val observer = object : Observer<Type> {
        override fun onChanged(it: Type) {
            procedure.invoke(it)
            removeObserver(this)
        }
    }

    observeForever(observer)
}

@MainThread
fun <Type1, Type2> LiveData<Holder2<Type1, Type2>>.once(procedure: Procedure2<Type1, Type2>) {
    val observer = object : Observer<Holder2<Type1, Type2>> {
        override fun onChanged(it: Holder2<Type1, Type2>) {
            procedure.invoke(it.value1, it.value2)
            removeObserver(this)
        }
    }

    observeForever(observer)
}
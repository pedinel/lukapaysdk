/*
 * Copyright (c) 2021. Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 27/8/21 4:29 p. m.
 */

package com.payco.lukasdk.lifecycle

import android.os.Looper
import androidx.lifecycle.MutableLiveData

/**
 * LiveData wrappers
 */
class LiveDatas {
    companion object {
        fun <Type> create(
            initializer: (MutableLiveData<Type>) -> Unit,
        ) : MutableLiveData<Type> {
            return ActiveLiveData(initializer)
        }

        fun <Type> from(
            value: Type,
        ) : LinkedLiveData<Type> {
            val result: LinkedLiveData<Type> = LinkedLiveData()
            if (Looper.myLooper() == Looper.getMainLooper()) {
                result.value = value
            } else {
                result.postValue(value)
            }
            return result
        }
    }
}
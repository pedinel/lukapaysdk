/*
 * Copyright (c) 2021. Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 10/9/21 2:15 p. m.
 */

package com.payco.lukasdk

import com.google.gson.annotations.SerializedName

enum class LukaCurrency(
    val iso: String,
    val symbol: String,
) {
    @SerializedName("USD")
    USD("USD", "$");
}
/*
 * Copyright (c) 2021. Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 9/9/21 10:46 a. m.
 */

package com.payco.lukasdk.transaction

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.payco.lukasdk.LukaApi
import com.payco.lukasdk.bridge.LukaBridgeResponse
import com.payco.lukasdk.bridge.LukaPaymentAction
import com.payco.lukasdk.lifecycle.Holder2
import com.payco.lukasdk.lifecycle.LinkedLiveData
import com.payco.lukasdk.lifecycle.LiveDatas
import com.payco.lukasdk.lifecycle.combineWith
import com.payco.lukasdk.services.auth.LukaSession
import com.payco.lukasdk.services.luka.LukaConfig

internal class LukaPaymentRequest(
    val mParams: LukaPaymentParams,
    api: LukaApi,
    config: LukaConfig,
    val mSession: LukaSession,
    val mAction: LukaPaymentAction
) {
    val mMethod = mParams.method
    val mGateway = mMethod.gateway()
    val mBridge = mGateway.bridge()
    val mSetupResult = mBridge.setup(api, config, mSession)
    private val mCallback = api.mCallbacks[mGateway]!!
    private val mActionResult = MutableLiveData<LukaBridgeResponse>()

    init {
        api.mLukaResponse[mGateway] = mActionResult
    }

    fun callback(): LinkedLiveData<Holder2<LukaPaymentRequest, Boolean>> {
        return LiveDatas.from(this)
            .combineWith(mSetupResult)
    }

    fun dispatch(): LiveData<LukaBridgeResponse> {
        mCallback.launch(this)
        return mActionResult
    }
}
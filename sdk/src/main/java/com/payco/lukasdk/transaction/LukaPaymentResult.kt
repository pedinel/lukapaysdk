/*
 * Copyright (c) 2021. Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 9/9/21 9:37 a. m.
 */

package com.payco.lukasdk.transaction

import com.payco.lukasdk.LukaCurrency
import com.payco.lukasdk.bridge.LukaMethod
import com.payco.lukasdk.services.luka.payment.LukaPayment

data class LukaPaymentResult(
    val method: LukaMethod,
    val amount: Double,
    val currency: LukaCurrency,
    val traceId: String,
    val charged: Boolean,
    val lukaCustomerId: String?,
) {
    constructor(data: LukaPayment) : this(
        LukaMethod.CreditCard,
        data.amount,
        data.currency,
        data.traceId,
        data.isSuccessful,
        data.cardOwner?.id,
    )

    override fun toString(): String {
        return "LukaPaymentResult(method=$method, amount=$amount, currency=$currency, traceId='$traceId', charged=$charged, customerId=$lukaCustomerId)"
    }
}
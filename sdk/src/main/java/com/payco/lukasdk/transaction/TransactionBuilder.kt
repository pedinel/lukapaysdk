/*
 * Copyright (c) 2021. Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 9/9/21 9:35 a. m.
 */

package com.payco.lukasdk.transaction

import androidx.annotation.CheckResult
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import com.payco.lukasdk.lifecycle.once

/**
 * Helper to register transaction callbacks
 */
abstract class TransactionBuilder<Input, Progress, Output>(
    private val mResponse: LiveData<Input>
) {
    protected var mOnSuccess: ((Output) -> Unit)? = null
    protected var mOnError: ((Throwable) -> Unit)? = null
    protected var mOnProgress: ((Progress) -> Unit)? = null

    /**
     * Registers the transaction successful callback
     */
    @CheckResult
    fun onSuccess(action: (Output) -> Unit): TransactionBuilder<Input, Progress, Output> {
        mOnSuccess = action
        return this
    }

    /**
     * Registers the transaction failure callback
     */
    @CheckResult
    fun onError(action: (Throwable) -> Unit): TransactionBuilder<Input, Progress, Output> {
        mOnError = action
        return this
    }

    /**
     * Registers the transaction failure callback
     */
    @CheckResult
    fun onProgress(action: (Progress) -> Unit): TransactionBuilder<Input, Progress, Output> {
        mOnProgress = action
        return this
    }

    /**
     * Launches the transaction
     */
    fun execute(owner: LifecycleOwner) {
        onBegin()
        mResponse.once(owner) {
            onExecute(it)
        }
    }

    protected open fun onBegin() {

    }

    protected abstract fun onExecute(input: Input)
}
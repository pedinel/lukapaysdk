/*
 * Copyright (c) 2021. Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 6/10/21 4:04 p. m.
 */

package com.payco.lukasdk.transaction

import com.payco.lukasdk.LukaCurrency
import com.payco.lukasdk.bridge.LukaMethod

class LukaCardVaultParams(
    val lukaCustomerId: String? = null,
    email: String,
) : LukaPaymentParams(
    method = LukaMethod.CreditCard,
    amount = 1.0,
    currency = LukaCurrency.USD,
    email = email,
    enable3dSecureAuthentication = true,
) {
}
/*
 * Copyright (c) 2021. Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 9/9/21 9:35 a. m.
 */

package com.payco.lukasdk.transaction

import androidx.lifecycle.LiveData
import com.payco.lukasdk.bridge.LukaPaymentStep
import com.payco.lukasdk.exceptions.TransactionSetupFailedException
import com.payco.lukasdk.lifecycle.*
import com.payco.lukasdk.services.luka.transaction.TransactionHandler

internal class PaymentTransactionBuilder(
    request: LiveData<LukaPaymentRequest?>
) : TransactionBuilder<LukaPaymentRequest?, LukaTransactionProgress, LukaPaymentResult>(request) {

    private val mPause = LiveDatas.from(false)

    private val mHandler: TransactionHandler = TransactionHandler(
        onPause = { mPause.value = true },
        onResume = { mPause.value = false })

    override fun onExecute(input: LukaPaymentRequest?) {
        if (input == null) {
            //setup failed to initialize
            mOnError?.invoke(TransactionSetupFailedException())
        } else {
            //TODO check if selected payment method is supported
            launchAction(LukaPaymentStep.BEGIN, input)
        }
    }

    override fun onBegin() {
        //Setup progress step
        mOnProgress?.invoke(LukaTransactionProgress(LukaPaymentStep.SETUP, null, mHandler))
    }

    fun launchAction(action: LukaPaymentStep, request: LukaPaymentRequest?, payload: Any? = null) {
        if (request == null) {
            //Setup failed
            mOnError?.invoke(TransactionSetupFailedException())
            return
        }

        mPause
            .whenCondition {
                //Checks if not paused. This only works for setup step
                !it
            }
            .single()
            .doOnNext {
                //Calls the onProgress procedure
                mOnProgress?.invoke(LukaTransactionProgress(action, request.mSession.id, mHandler))
            }
            .whenCondition {
                //Checks if not paused.
                !it
            }
            .flatMap {
                //Builds the launch callback
                request.mBridge.launch(action, request, payload)
            }
            .once {
                //Handles the action result
                when (it.action) {
                    LukaPaymentStep.FINISH_SUCCESS -> {
                        //Finishes with success result
                        mOnSuccess?.invoke(it.payload as LukaPaymentResult)
                    }
                    LukaPaymentStep.FINISH_ERROR -> {
                        //Finishes with error result
                        mOnError?.invoke(it.payload as Throwable)
                    }
                    else -> {
                        //Next step
                        launchAction(it.action, request, it.payload)
                    }
                }
            }
    }
}
/*
 * Copyright (c) 2021. Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 6/10/21 4:04 p. m.
 */

package com.payco.lukasdk.transaction

import com.payco.lukasdk.LukaCurrency
import com.payco.lukasdk.bridge.LukaMethod
import com.payco.lukasdk.services.luka.payment.CreditCard

class LukaCardPaymentParams(
    val lukaCustomerId: String,
    val card: CreditCard,
    amount: Double,
    currency: LukaCurrency,
    email: String,
    customTraceId: String? = null,
) : LukaPaymentParams(
    method = LukaMethod.CreditCard,
    amount = amount,
    currency = currency,
    email = email,
    enable3dSecureAuthentication = false,
    customTraceId = customTraceId,
) {
}
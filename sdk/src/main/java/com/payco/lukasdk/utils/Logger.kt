/*
 * Copyright (c) 2021. Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 27/8/21 9:56 a. m.
 */

package com.payco.lukasdk.utils

import java.util.*
import kotlin.reflect.KClass

/**
 * Logs Handler
 */
class Logger private constructor(clazz: KClass<*>, private val dispatcher: LogDispatcher) {
    private val tag: String = clazz.simpleName ?: "AnonymousClass"

    fun debug(format: String, vararg parameters: Any) {
        try {
            val txt = formatText(format, parameters)
            dispatcher.d(tag, txt)
        } catch (t: Throwable) {
            t.printStackTrace()
        }
    }

    fun info(format: String, vararg parameters: Any) {
        try {
            val txt = formatText(format, parameters)
            dispatcher.i(tag, txt)
        } catch (t: Throwable) {
            t.printStackTrace()
        }
    }

    fun warn(format: String, vararg parameters: Any) {
        try {
            val txt = formatText(format, parameters)
            dispatcher.w(tag, txt)
        } catch (t: Throwable) {
            t.printStackTrace()
        }
    }

    fun warn(t: Throwable, format: String, vararg parameters: Any) {
        try {
            val txt = formatText(format, parameters)
            dispatcher.w(t, tag, txt)
        } catch (u: Throwable) {
            u.printStackTrace()
        }
    }

    fun error(format: String, vararg parameters: Any) {
        try {
            val txt = formatText(format, parameters)
            dispatcher.e(tag, txt)
        } catch (t: Throwable) {
            t.printStackTrace()
        }
    }

    fun error(t: Throwable, format: String, vararg parameters: Any) {
        try {
            val txt = formatText(format, parameters) + '\n' + getThrowableError(t)
            dispatcher.e(t, tag, txt)
        } catch (u: Throwable) {
            u.printStackTrace()
        }
    }

    private fun formatText(format: String, vararg parameters: Any?): String {
        return if (parameters.isEmpty()) {
            format
        } else {
            String.format(Locale.ROOT, format, *parameters)
        }
    }

    companion object {
        private lateinit var sDispatcher: LogDispatcher

        private fun dispatcher(): LogDispatcher {
            try {
                Class.forName("com.payco.lukasdk.test.utils.Logger")
                return TestLogDispatcher()
            } catch (ignored: ClassNotFoundException) {
            }

            return LogDispatcher()
        }


        /**
         * Creates and retrieves a new Logger Instance

         * @param clazz the Class to log
         * @return the Logger Instance
         */
        fun getLogger(clazz: KClass<*>): Logger {
            if (!::sDispatcher.isInitialized) {
                sDispatcher = dispatcher()
            }
            return Logger(clazz, sDispatcher)
        }

        fun getThrowableError(t: Throwable): String {
            val sb = StringBuilder()
            sb.append(t.message)
            appendCause(sb, t)
            var cause = t.cause
            while (cause != null) {
                sb.append(" cause: ")
                sb.append(cause.message)
                appendCause(sb, cause)
                cause = cause.cause
            }
            return sb.toString()
        }

        private fun appendCause(sb: StringBuilder, t: Throwable) {
            val trace = t.stackTrace
            for (traceElement in trace) sb.append("\tat ").append(traceElement).append('\n')
        }
    }

}
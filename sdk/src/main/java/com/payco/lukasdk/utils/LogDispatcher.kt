/*
 * Copyright (c) 2021. Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 3/9/21 3:32 p. m.
 */

package com.payco.lukasdk.utils

import android.util.Log

/**
 * Writes log messages
 */
internal open class LogDispatcher {
    open fun d(tag: String, message: String) {
        Log.d(tag, message)
    }

    open fun d(t: Throwable, tag: String, message: String) {
        Log.d(tag, message, t)
    }

    open fun i(tag: String, message: String) {
        Log.i(tag, message)
    }

    open fun i(t: Throwable, tag: String, message: String) {
        Log.i(tag, message, t)
    }

    open fun w(tag: String, message: String) {
        Log.w(tag, message)
    }

    open fun w(t: Throwable, tag: String, message: String) {
        Log.w(tag, message, t)
    }

    open fun e(tag: String, message: String) {
        Log.e(tag, message)
    }

    open fun e(t: Throwable, tag: String, message: String) {
        Log.e(tag, message, t)
    }

    protected fun throwableMessage(t: Throwable): String {
        val sb = StringBuilder()
        sb.append(t.message)
        appendCause(sb, t)
        var cause = t.cause
        while (cause != null) {
            sb.append(" cause: ")
            sb.append(cause.message)
            appendCause(sb, cause)
            cause = cause.cause
        }
        return sb.toString()
    }

    protected fun appendCause(sb: StringBuilder, t: Throwable) {
        val trace: Array<StackTraceElement>? = t.stackTrace

        if (trace != null) {
            for (traceElement in trace)
                sb.append("\tat ")
                    .append(traceElement)
                    .append("\n")
        }
    }
}
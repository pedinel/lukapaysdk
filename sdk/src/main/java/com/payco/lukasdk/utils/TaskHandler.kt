/*
 * Copyright (c) 2021. Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 30/8/21 11:47 a. m.
 */

package com.payco.lukasdk.utils

import android.os.Handler
import android.os.Looper

/**
 * Handles a task execution
 */
class TaskHandler(
    private val task: Runnable
) {
    private val handler = Handler(Looper.getMainLooper())

    fun post(removeCallbacks: Boolean = true) {
        if (removeCallbacks) {
            handler.removeCallbacks(task)
        }
        handler.post(task)
    }

    fun postDelayed(delayInMillis: Long, removeCallbacks: Boolean = true) {
        if (removeCallbacks) {
            handler.removeCallbacks(task)
        }
        handler.postDelayed(task, delayInMillis)
    }

    fun cancel() {
        handler.removeCallbacks(task)
    }
}
/*
 * Copyright (c) 2021. Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 6/9/21 1:23 p. m.
 */

package com.payco.lukasdk.services.luka.payment


import com.google.gson.annotations.SerializedName
import com.payco.lukasdk.LukaCurrency

/**
 * Luka credit card model
 */
data class CreditCard(
    @SerializedName("Bin")
    val bin: String? = null,

    @SerializedName("CategoriaTarjeta")
    val category: String?,

    @SerializedName("Ciudad")
    val ciudad: String? = null,

    @SerializedName("Descripcion")
    val description: String? = "",

    @SerializedName("Direccion")
    val address: Address? = null,

    @SerializedName("CodigoPostal")
    val zipCode: String? = "",

    @SerializedName("EstaBoveda")
    val thisVault: Boolean? = null,

    @SerializedName("Estado")
    val state: Any? = null,

    @SerializedName("FechaVencimiento")
    val expiresAt: String?,

    @SerializedName("Id")
    val id: Long = 0L,

    @SerializedName("IdStatus")
    val statusId: Int? = null,

    @SerializedName("Moneda")
    val currency: LukaCurrency? = null,

    @SerializedName("Pais")
    val countryCode: String?,

    @SerializedName("SubTipoTarjeta")
    val subType: String?,

    @SerializedName("TipoTarjeta")
    val type: String,

    @SerializedName("UltimosCuatroDigitos")
    val last4: String
)
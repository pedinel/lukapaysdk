/*
 * Copyright (c) 2021. Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 6/9/21 11:18 a. m.
 */

package com.payco.lukasdk.services.luka

import com.payco.lukasdk.http.ApiRequest
import com.payco.lukasdk.services.luka.bluesnap.BluesnapTransaction
import com.payco.lukasdk.services.luka.payment.CreditCard
import com.payco.lukasdk.services.luka.payment.LukaPayment
import com.payco.lukasdk.services.luka.transaction.AuthorizedTransaction
import com.payco.lukasdk.services.luka.transaction.LukaTransaction
import retrofit2.http.*

internal interface Api {
    /**
     * Retrieves the api info and authorized payment channels
     */
    @GET("servicio/config")
    fun config(@Header("Authorization") auth: String): ApiRequest<LukaConfig>

    /**
     * Retrieves the api info and authorized payment channels
     */
    @GET("transaccion/token")
    fun token(@Header("Authorization") auth: String): ApiRequest<Map<String, Any>>

    /**
     * Registers a new transaction to Luka.
     */
    @POST("transaccion/mobile")
    fun authorizeTransaction(@Header("Authorization") auth: String, @Body transaction: LukaTransaction): ApiRequest<AuthorizedTransaction>

    /**
     * Updates the transaction status with the payment result from Bluesnap.
     */
    @PUT("transaccion/mobile")
    fun updateTransaction(@Header("Authorization") auth: String, @Body payload: BluesnapTransaction): ApiRequest<LukaPayment>

    /**
     * Registers a new transaction to Luka.
     */
    @POST("transaccion")
    fun chargeTransaction(
        @Header("Authorization") auth: String,
        @Body transaction: LukaTransaction
    ): ApiRequest<LukaPayment>

    /**
     * Retrieve luka payments by trazaId.
     */
    @GET("transaccion")
    fun checkTransaction(
        @Header("Authorization") auth: String,
        @Query("trazaId") traceId: String
    ): ApiRequest<List<LukaPayment>>

    /**
     * Retrieve stored cards
     */
    @GET("tarjetacredito/servicio/{lukaCustomerId}")
    fun indexCustomerCards(
        @Header("Authorization") auth: String,
        @Path("lukaCustomerId") lukaCustomerId: String
    ): ApiRequest<List<CreditCard>>

    /**
     * Deletes a vault card from Luka.
     */
    @DELETE("tarjetacredito/{cardId}/user/{lukaCustomerId}")
    fun deleteCustomerCard(
        @Header("Authorization") auth: String,
        @Path("cardId") cardId: Long,
        @Path("lukaCustomerId") lukaCustomerId: String
    ): ApiRequest<Void>

}
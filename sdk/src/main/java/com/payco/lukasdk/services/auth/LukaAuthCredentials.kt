/*
 * Copyright (c) 2021. Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 30/8/21 3:18 p. m.
 */

package com.payco.lukasdk.services.auth

import com.google.gson.annotations.SerializedName
import okhttp3.Credentials

/**
 * Credentials to login into Luka Services using [Auth.login] endpoint.
 */
class LukaAuthCredentials(
    /**
     * Username provided by Luka
     */
    @SerializedName("Username")
    var user: String = "",

    /**
     * Password provided by Luka
     */
    @SerializedName("Password")
    var password: String = "",
) {
    internal val basic: String
        get() = Credentials.basic(user, password)
}
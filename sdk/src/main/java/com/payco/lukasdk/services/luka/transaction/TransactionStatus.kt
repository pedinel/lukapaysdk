/*
 * Copyright (c) 2021. Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 6/9/21 1:02 p. m.
 */

package com.payco.lukasdk.services.luka.transaction

import com.google.gson.annotations.SerializedName

/**
 * Transaction statuses
 */
enum class TransactionStatus {
    @SerializedName("Tx En Proceso")
    InProgress,

    @SerializedName("Tx Existosa")
    Sucessful,

    @SerializedName("Tx Fallida")
    Failed,
}
/*
 * Copyright (c) 2021. Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 6/9/21 12:41 p. m.
 */

package com.payco.lukasdk.services.luka.transaction


import com.google.gson.annotations.SerializedName
import com.payco.lukasdk.LukaCurrency
import com.payco.lukasdk.services.luka.CardOwner
import com.payco.lukasdk.services.luka.payment.CreditCard

/**
 * Luka transaction payload
 */
data class LukaTransaction(
    @SerializedName("EmailTarjetaHabiente")
    val cardOwnerEmail: String?,

    @SerializedName("IdCanal")
    val channelId: Int = 4,//Android channel

    @SerializedName("IdTraza")
    val traceId: String,

    @SerializedName("Moneda")
    val currency: LukaCurrency?,

    @SerializedName("Monto")
    val amount: Double?,

    @SerializedName("Referencia")
    val ref: String? = "",

    @SerializedName("TarjetaCredito")
    val creditCard: CreditCard? = null,

    @SerializedName("TarjetaHabiente")
    val creditCardOwner: CardOwner? = null,

    @SerializedName("TokenBluesnap")
    val bluesnapToken: String? = null,

    @SerializedName("ValidacionTarjeta")
    val cardValidation: Boolean = false,

    @SerializedName("IdTarjetaCredito")
    val creditCardId: Long? = null,
)
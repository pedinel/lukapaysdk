/*
 * Copyright (c) 2021. Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 6/9/21 12:41 p. m.
 */

package com.payco.lukasdk.services.luka.payment


import com.google.gson.annotations.SerializedName

/**
 * Card owner
 */
data class LukaPayer(
    @SerializedName("Apellido")
    val lastName: String,

    @SerializedName("Nombre")
    val name: String,

    @SerializedName("Email")
    val email: String
)
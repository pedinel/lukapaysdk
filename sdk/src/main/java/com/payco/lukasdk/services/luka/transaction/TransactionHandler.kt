/*
 * Copyright (c) 2021. Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 14/9/21 10:32 a. m.
 */

package com.payco.lukasdk.services.luka.transaction

class TransactionHandler(private val onPause: () -> Unit, private val onResume: () -> Unit) {
    fun pause() {
        onPause()
    }

    fun resume() {
        onResume()
    }
}
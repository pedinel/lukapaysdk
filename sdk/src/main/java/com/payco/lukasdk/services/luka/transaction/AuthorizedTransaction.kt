/*
 * Copyright (c) 2021. Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 6/9/21 12:56 p. m.
 */

package com.payco.lukasdk.services.luka.transaction


import com.google.gson.annotations.SerializedName
import com.payco.lukasdk.LukaCurrency
import java.util.*

/**
 * Luka Transaction
 */
data class AuthorizedTransaction(
    @SerializedName("Estatus")
    val status: TransactionStatus,

    @SerializedName("Fecha")
    val date: Date,

    @SerializedName("Id")
    val id: Long,

    @SerializedName("IdEstatus")
    val statusId: Int,

    @SerializedName("Moneda")
    val currency: LukaCurrency,

    @SerializedName("Monto")
    val amount: Double,

    @SerializedName("NombreServicio")
    val serviceName: String,

    @SerializedName("NumeroAutorizaion")
    val authorizationNumber: String?,

    @SerializedName("NumeroTarjeta")
    val cardNumber: String
)
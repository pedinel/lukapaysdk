/*
 * Copyright (c) 2021. Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 10/9/21 10:26 a. m.
 */

package com.payco.lukasdk.services.luka.bluesnap

internal data class BluesnapAuth (
    val userId: String,
    val token: String,
)
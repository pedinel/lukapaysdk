/*
 * Copyright (c) 2021. Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 6/9/21 1:23 p. m.
 */

package com.payco.lukasdk.services.luka.payment


import com.google.gson.annotations.SerializedName

/**
 * Luka credit card model
 */
data class Address(
    @SerializedName("Ciudad")
    val ciudad: String? = null,

    @SerializedName("Direccion")
    val address: String? = "",

    @SerializedName("CodigoPostal")
    val zipCode: String? = "",

    @SerializedName("Estado")
    val state: Any? = null,

    @SerializedName("IdPais")
    val countryId: Long?,
)
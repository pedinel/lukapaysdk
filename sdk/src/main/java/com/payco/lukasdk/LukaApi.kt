/*
 * Copyright (c) 2021. Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 26/8/21 4:23 p. m.
 */

package com.payco.lukasdk

import android.content.Context
import androidx.activity.result.ActivityResultCaller
import androidx.activity.result.ActivityResultLauncher
import androidx.annotation.CheckResult
import androidx.annotation.MainThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.payco.lukasdk.bridge.LukaBridgeResponse
import com.payco.lukasdk.bridge.LukaGateway
import com.payco.lukasdk.bridge.LukaPaymentAction
import com.payco.lukasdk.exceptions.LukaHttpException
import com.payco.lukasdk.exceptions.PaymentNotFoundException
import com.payco.lukasdk.http.ApiErrorResponse
import com.payco.lukasdk.http.ApiResponse
import com.payco.lukasdk.http.LukaServiceProvider
import com.payco.lukasdk.lifecycle.*
import com.payco.lukasdk.services.auth.LukaAuthCredentials
import com.payco.lukasdk.services.auth.LukaSession
import com.payco.lukasdk.services.luka.LukaConfig
import com.payco.lukasdk.services.luka.bluesnap.BluesnapAuth
import com.payco.lukasdk.services.luka.payment.CreditCard
import com.payco.lukasdk.services.luka.payment.LukaPayment
import com.payco.lukasdk.services.luka.transaction.AuthorizedTransaction
import com.payco.lukasdk.services.luka.transaction.LukaTransaction
import com.payco.lukasdk.transaction.*
import retrofit2.Retrofit
import java.util.*
import java.util.concurrent.TimeUnit

open class LukaApi(
    internal val mContext: Context,
    internal val mGson: Gson,
    lukaRetrofit: Retrofit,
    testModeEnabled: Boolean,
) {
    private val mLukaServiceProvider = LukaServiceProvider(lukaRetrofit)
    private val mCredentials = LukaAuthCredentials()
    internal var mSession: LukaSession? = null
    internal val mTestMode: Boolean = testModeEnabled
    internal lateinit var mLukaResponse : EnumMap<LukaGateway, MutableLiveData<LukaBridgeResponse>>
    internal lateinit var mCallbacks: EnumMap<LukaGateway, ActivityResultLauncher<LukaPaymentRequest>>

    private fun session(): LiveData<LukaSession> {
        val authRequest = mLukaServiceProvider.auth()
            .login(mCredentials)
            .withAutoLaunch(10, TimeUnit.MINUTES)
            .withAutoRefresh(5, TimeUnit.MINUTES)
            .withAutoRetry(6, TimeUnit.SECONDS)

        val session = authRequest.response.then { response, next: Next<LukaSession> ->
            response.headers()?.let {
                val id = it["id"]
                val token = it["token"]

                if (id != null && token != null) {
                    next.value(LukaSession(id, token, response.updateTime))
                }
            }
        }

        return session.doOnNext {
            mSession = it
        }
    }

    private fun config(): LiveData<Holder2<LukaConfig, LukaSession>> {
        val config = session().flatMap {
            val result: LinkedLiveData<Holder2<LukaConfig, LukaSession>> = mLukaServiceProvider.api()
                .config(it.bearerToken)
                .withAutoLaunch()
                .withAutoRetry(6, TimeUnit.SECONDS)
                .data
                .combineWith(LiveDatas.from(it))
            result
        }

        return config.then { sessionConfig, next ->
            val isSessionInvalid = sessionConfig?.value2?.hasExpired ?: true

            if (!isSessionInvalid) {
                next.value(sessionConfig)
            }
        }
    }

    internal fun authenticate(credentials: LukaAuthCredentials) {
        mCredentials.user = credentials.user
        mCredentials.password = credentials.password
    }

    internal fun bluesnapAuth(session: LukaSession): LiveData<BluesnapAuth> {
        return mLukaServiceProvider.api()
            .token(session.bearerToken)
            .withAutoLaunch()
            .withAutoRetry(6, TimeUnit.SECONDS)
            .response
            .then { response, next ->
                response.headers()?.let {
                    val token = it["bstoken"]

                    if (token != null) {
                        next.value(BluesnapAuth(session.id, token))
                    }
                }
            }
    }

    @MainThread
    internal fun setup(caller: ActivityResultCaller) {
        mLukaResponse = EnumMap<LukaGateway, MutableLiveData<LukaBridgeResponse>>(LukaGateway::class.java)

        val launcherMap = EnumMap<LukaGateway, ActivityResultLauncher<LukaPaymentRequest>>(LukaGateway::class.java)
        mCallbacks = launcherMap.apply {
            LukaGateway.values().forEach { gateway ->
                //Register activity results
                val registerForActivityResult = caller.registerForActivityResult(gateway.resolver()) {
                    mLukaResponse[gateway]?.value = it
                }

                put(gateway, registerForActivityResult)
            }
        }
    }

    fun clear() {
        mCallbacks = EnumMap(LukaGateway::class.java)
    }

    /**
     * Creates a new payment request
     */
    @MainThread
    @CheckResult
    fun createPaymentRequest(params: LukaPaymentParams): TransactionBuilder<out Any?, LukaTransactionProgress, LukaPaymentResult> {
        val requestData = config().flatMap { config, session ->
            val action : LukaPaymentAction = when (params) {
                is LukaCardVaultParams -> LukaPaymentAction.CARD_STORAGE
                is LukaCardPaymentParams -> LukaPaymentAction.CARD_SELECTION_CHARGE
                else -> LukaPaymentAction.ANONYMOUS_CHARGE
            }

            LukaPaymentRequest(params, this, config, session, action)
                .callback()
        }.map { req: LukaPaymentRequest?, isSuccessful ->
            if (isSuccessful) {
                req
            } else {
                null
            }
        }.allowNulls()

        return PaymentTransactionBuilder(requestData)
    }

    /**
     * Adds new card to Vault. If lukaCustomerId is null, the customer will be created. Otherwise,
     * the card will be added to the customer's vault.
     */
    @MainThread
    @CheckResult
    fun addCustomerCardRequest(email: String, lukaCustomerId: String? = null): TransactionBuilder<out Any?, LukaTransactionProgress, LukaPaymentResult> {
        val params = LukaCardVaultParams(
            lukaCustomerId = lukaCustomerId,
            email = email,
        )

        return createPaymentRequest(params)
    }

    /**
     * Lists all cards from Vault
     */
    @MainThread
    @CheckResult
    fun indexCustomerCardsRequest(lukaCustomerId: String): TransactionBuilder<ApiResponse<List<CreditCard>>, Any, List<CreditCard>> {
        val requestData = session().flatMap { session ->
            mLukaServiceProvider.api()
                .indexCustomerCards(session.bearerToken, lukaCustomerId)
                .withAutoLaunch()
                .response
        }

        return object : TransactionBuilder<ApiResponse<List<CreditCard>>, Any, List<CreditCard>>(requestData) {
            override fun onExecute(input: ApiResponse<List<CreditCard>>) {
                when {
                    input.isSuccessful -> {
                        val result = input.data()

                        mOnSuccess?.invoke(result)
                    }
                    input.hasError() -> {
                        try {
                            mOnError?.invoke(LukaHttpException(input.error(ApiErrorResponse::class)))
                        } catch (e: Exception) {
                            mOnError?.invoke(e)
                        }
                    }
                    input.hasFailed() -> {
                        mOnError?.invoke(input.exception()!!)
                    }
                }
            }
        }
    }

    /**
     * Deletes a card from Vault
     */
    @MainThread
    @CheckResult
    fun deleteCustomerCardsRequest(lukaCustomerId: String, cardId: Long): TransactionBuilder<ApiResponse<Void>, Any, Boolean> {
        val requestData = session().flatMap { session ->
            mLukaServiceProvider.api()
                .deleteCustomerCard(session.bearerToken, cardId, lukaCustomerId)
                .withAutoLaunch()
                .response
        }

        return object : TransactionBuilder<ApiResponse<Void>, Any, Boolean>(requestData) {
            override fun onExecute(input: ApiResponse<Void>) {
                when {
                    input.isSuccessful -> {
                        mOnSuccess?.invoke(true)
                    }
                    input.hasError() -> {
                        try {
                            mOnError?.invoke(LukaHttpException(input.error(ApiErrorResponse::class)))
                        } catch (e: Exception) {
                            mOnError?.invoke(e)
                        }
                    }
                    input.hasFailed() -> {
                        mOnError?.invoke(input.exception()!!)
                    }
                }
            }
        }
    }

    @MainThread
    @CheckResult
    internal fun prepareTransaction(transaction: LukaTransaction): ApiTransactionBuilder<AuthorizedTransaction, LukaBridgeResponse> {
        val response = session().flatMap {
            mLukaServiceProvider.api()
                .authorizeTransaction(it.bearerToken, transaction)
                .withAutoLaunch()
                .response
        }

        return ApiTransactionBuilder(response)
    }

    @MainThread
    @CheckResult
    internal fun chargeTransaction(session: LukaSession, transaction: LukaTransaction): ApiTransactionBuilder<LukaPayment, LukaBridgeResponse> {
        val response = mLukaServiceProvider.api()
            .chargeTransaction(session.bearerToken, transaction)
            .withAutoLaunch()
            .response

        return ApiTransactionBuilder(response)
    }

    @MainThread
    @CheckResult
    fun checkTransaction(traceId: String): TransactionBuilder<out Any?, out Any?, LukaPaymentResult> {
        val response = mLukaServiceProvider.api()
            .checkTransaction(mCredentials.basic, traceId)
            .withAutoLaunch()
            .response

        return object : TransactionBuilder<ApiResponse<List<LukaPayment>>, Any, LukaPaymentResult>(response) {
            override fun onExecute(input: ApiResponse<List<LukaPayment>>) {
                when {
                    input.isSuccessful -> {
                        val result = input.data()

                        if (result.isEmpty()) {
                            mOnError?.invoke(PaymentNotFoundException())
                        } else {
                            mOnSuccess?.invoke(LukaPaymentResult(result.first()))
                        }
                    }
                    input.hasError() -> {
                        try {
                            mOnError?.invoke(LukaHttpException(input.error(ApiErrorResponse::class)))
                        } catch (e: Exception) {
                            mOnError?.invoke(e)
                        }
                    }
                    input.hasFailed() -> {
                        mOnError?.invoke(input.exception()!!)
                    }
                }
            }
        }
    }
}
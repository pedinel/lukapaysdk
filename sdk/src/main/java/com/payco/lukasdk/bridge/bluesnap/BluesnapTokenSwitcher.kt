/*
 * Copyright (c) 2021. Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 8/9/21 2:20 p. m.
 */

package com.payco.lukasdk.bridge.bluesnap

import androidx.lifecycle.Observer
import com.payco.lukasdk.LukaApi
import com.payco.lukasdk.lifecycle.LinkedLiveData
import com.payco.lukasdk.lifecycle.delay
import com.payco.lukasdk.lifecycle.once
import com.payco.lukasdk.services.auth.LukaSession
import com.payco.lukasdk.services.luka.bluesnap.BluesnapAuth
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicReference

/**
 * Handles and renews tokens for BlueSnap
 */
internal class BluesnapTokenSwitcher(
    /**
     * Luka API Instance
     */
    private val mApi: LukaApi,

    /**
     * Luka active session
     */
    private val mSession: LukaSession,

    /**
     * Token callback
     */
    private val mCallback: (String) -> Unit

) : Observer<BluesnapAuth> {

    /**
     * Token holder observable
     */
    internal val mTokenHolder = LinkedLiveData<String>()

    /**
     * API Request to renew tokens with
     */
    private val mSource = AtomicReference(mApi.bluesnapAuth(mSession))

    /**
     * Checks if the current instance is active
     */
    private var mActive = true

    init {
        //triggers first token request call
        mTokenHolder.addSource(mSource.get(), this)
    }

    override fun onChanged(auth: BluesnapAuth?) {
        if (auth != null) {
            //A new token was emitted. It is sent to mTokenHolder
            mTokenHolder.value = auth.token

            //Invalidate previous ApiRequest source for token renewals
            mTokenHolder.removeSource(mSource.get())

            if (mActive) {
                //Adds a new Token renewal source if this is still active
                mSource.set(mApi.bluesnapAuth(mSession))
                mTokenHolder.addSource(mSource.get(), this@BluesnapTokenSwitcher)
            } else {
                //clear memory
                mSource.set(null)
            }
        }
    }

    fun recycle() {
        //The tokens last for 1 hour. This launches the token renewal procedure after 45 minutes.
        mTokenHolder.delay(45, TimeUnit.MINUTES).once {
            //sends back the renewed token
            mCallback(it)
        }
    }

    fun close() {
        mActive = false
    }
}
/*
 * Copyright (c) 2021. Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 9/9/21 3:20 p. m.
 */

package com.payco.lukasdk.bridge.bluesnap

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.activity.result.contract.ActivityResultContract
import com.bluesnap.androidapi.models.SdkResult
import com.bluesnap.androidapi.views.activities.BluesnapCheckoutActivity
import com.bluesnap.androidapi.views.activities.BluesnapChoosePaymentMethodActivity
import com.payco.lukasdk.LukaError
import com.payco.lukasdk.bridge.LukaBridgeResponse
import com.payco.lukasdk.bridge.LukaPaymentAction
import com.payco.lukasdk.bridge.LukaPaymentStep
import com.payco.lukasdk.exceptions.PaymentFailedException
import com.payco.lukasdk.transaction.LukaPaymentRequest

internal class BlueSnapResolver : ActivityResultContract<LukaPaymentRequest, LukaBridgeResponse>() {
    override fun createIntent(context: Context, input: LukaPaymentRequest?): Intent {
        return when (input!!.mAction) {
            LukaPaymentAction.NATIVE_CHOOSE -> {
                Intent(context.applicationContext, BluesnapChoosePaymentMethodActivity::class.java)
            }
            else -> Intent(context.applicationContext, BluesnapCheckoutActivity::class.java)
        }
    }

    override fun parseResult(resultCode: Int, intent: Intent?): LukaBridgeResponse {
        if (resultCode != BluesnapCheckoutActivity.BS_CHECKOUT_RESULT_OK &&
            resultCode != Activity.RESULT_OK &&
            resultCode != BluesnapChoosePaymentMethodActivity.BS_CHOOSE_PAYMENT_METHOD_RESULT_OK) {
            if (intent != null) {
                val description = intent.getStringExtra(BluesnapCheckoutActivity.SDK_ERROR_MSG)
                return LukaBridgeResponse(
                    LukaPaymentStep.FINISH_ERROR,
                    PaymentFailedException(LukaError.SDK_FAILED, description)
                )
            } else {
                // User aborted the checkout process
                return LukaBridgeResponse(
                    LukaPaymentStep.FINISH_ERROR,
                    PaymentFailedException(LukaError.ABORTED_BY_USER)
                )
            }
        }

        // Here we can access the payment result
        val sdkResult : SdkResult = intent?.getParcelableExtra(BluesnapCheckoutActivity.EXTRA_PAYMENT_RESULT)
            ?: return LukaBridgeResponse(
                LukaPaymentStep.FINISH_ERROR,
                PaymentFailedException(LukaError.SDK_FAILED)
            )

        // this result will be returned from both BluesnapCheckoutActivity and BluesnapCreatePaymentActivity,
        // since handling is the same. BluesnapChoosePaymentMethodActivity has a different OK result code.
        if (BluesnapCheckoutActivity.BS_CHECKOUT_RESULT_OK == sdkResult.result) {
            // Call app server to process the payment
            return LukaBridgeResponse(
                LukaPaymentStep.PREPARE,
                sdkResult
            )
        } else {
            return LukaBridgeResponse(
                LukaPaymentStep.FINISH_ERROR,
                PaymentFailedException(LukaError.SDK_FAILED, "another ok code?")//TODO read and fix
            )
        }
    }
}
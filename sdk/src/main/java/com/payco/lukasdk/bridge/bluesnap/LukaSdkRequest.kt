/*
 * Copyright (c) 2021. Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 19/11/21 4:20 p. m.
 */
package com.payco.lukasdk.bridge.bluesnap

import android.view.View
import com.bluesnap.androidapi.models.SdkRequest

internal class LukaSdkRequest(
    amount: Double,
    currencyIso: String,
    emailRequired: Boolean,
    private val mCustomButtonText: String? = null,
) : SdkRequest(
    amount,
    currencyIso,
    false,
    emailRequired,
    false
) {
    override fun getBuyNowButtonText(view: View?): String {
        return mCustomButtonText ?: super.getBuyNowButtonText(view)
    }
}
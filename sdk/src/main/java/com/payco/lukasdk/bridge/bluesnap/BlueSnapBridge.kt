/*
 * Copyright (c) 2021. Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 8/9/21 2:08 p. m.
 */

package com.payco.lukasdk.bridge.bluesnap

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.bluesnap.androidapi.models.SdkRequestShopperRequirements
import com.bluesnap.androidapi.models.SdkResult
import com.bluesnap.androidapi.services.BlueSnapService
import com.bluesnap.androidapi.services.BluesnapServiceCallback
import com.bluesnap.androidapi.services.TokenProvider
import com.bluesnap.androidapi.services.TokenServiceCallback
import com.payco.lukasdk.LukaApi
import com.payco.lukasdk.LukaCurrency
import com.payco.lukasdk.LukaError
import com.payco.lukasdk.bridge.LukaBridge
import com.payco.lukasdk.bridge.LukaBridgeResponse
import com.payco.lukasdk.bridge.LukaPaymentAction
import com.payco.lukasdk.bridge.LukaPaymentStep
import com.payco.lukasdk.exceptions.CardAlreadyRegisteredException
import com.payco.lukasdk.exceptions.LukaHttpException
import com.payco.lukasdk.exceptions.PaymentFailedException
import com.payco.lukasdk.lifecycle.LiveDatas
import com.payco.lukasdk.lifecycle.once
import com.payco.lukasdk.services.auth.LukaSession
import com.payco.lukasdk.services.luka.CardOwner
import com.payco.lukasdk.services.luka.LukaConfig
import com.payco.lukasdk.services.luka.payment.CreditCard
import com.payco.lukasdk.services.luka.transaction.LukaTransaction
import com.payco.lukasdk.transaction.LukaCardPaymentParams
import com.payco.lukasdk.transaction.LukaCardVaultParams
import com.payco.lukasdk.transaction.LukaPaymentRequest
import com.payco.lukasdk.transaction.LukaPaymentResult

/**
 * BlueSnap bridge from Luka
 */
internal class BlueSnapBridge : LukaBridge {
    /**
     * LukaApi instance
     */
    private lateinit var mApi: LukaApi

    /**
     * Current active Session
     */
    private lateinit var mSession: LukaSession

    /**
     * Token manager
     */
    private lateinit var mTokenSwitcher: BluesnapTokenSwitcher

    /**
     * Result callback for setup
     */
    private lateinit var mSetupResult: MutableLiveData<Boolean>

    /**
     * Current valid token
     */
    private lateinit var mToken: String

    /**
     * BlueSnap token Provider. Provides a new token when the previous one expires
     */
    private fun tokenProvider(): TokenProvider {
        return object: TokenProvider {
            override fun getNewToken(tokenServiceCallback: TokenServiceCallback?) {
                //sends tue current token Back
                tokenServiceCallback?.complete(mToken)

                //triggers the token renewal procedure
                mTokenSwitcher.recycle()
            }
        }
    }

    /**
     * Setup response callback
     */
    private fun serviceCallback(): BluesnapServiceCallback {
        return object: BluesnapServiceCallback {
            override fun onSuccess() {
                mSetupResult.postValue(true)
            }

            override fun onFailure() {
                mSetupResult.postValue(false)
            }

        }
    }

    override fun launch(
        step: LukaPaymentStep,
        request: LukaPaymentRequest,
        payload: Any?
    ): LiveData<LukaBridgeResponse> {
        return when (request.mAction) {
            LukaPaymentAction.ANONYMOUS_CHARGE -> {
                when (step) {
                    LukaPaymentStep.BEGIN -> anonymousPaymentBegin(request)
                    LukaPaymentStep.PREPARE -> paymentPrepare(request, payload as SdkResult)
                    LukaPaymentStep.CHARGE -> anonymousPaymentCharge(request, payload as LukaTransaction)
                    else -> throw RuntimeException("Unknown action ${request.mAction}::$step for bridge {$javaClass}")
                }
            }
            LukaPaymentAction.CARD_STORAGE -> {
                when (step) {
                    LukaPaymentStep.BEGIN -> anonymousPaymentBegin(request)
                    LukaPaymentStep.PREPARE -> paymentPrepare(request, payload as SdkResult)
                    LukaPaymentStep.CHARGE -> anonymousPaymentCharge(request, payload as LukaTransaction)
                    else -> throw RuntimeException("Unknown action ${request.mAction}::$step for bridge {$javaClass}")
                }
            }
            LukaPaymentAction.CARD_SELECTION_CHARGE -> {
                when (step) {
                    LukaPaymentStep.BEGIN -> cardPaymentBegin(request)
                    LukaPaymentStep.PREPARE -> paymentPrepare(request, payload as SdkResult)
                    LukaPaymentStep.CHARGE -> anonymousPaymentCharge(request, payload as LukaTransaction)
                    else -> throw RuntimeException("Unknown action ${request.mAction}::$step for bridge {$javaClass}")
                }
            }
            LukaPaymentAction.NATIVE_CHOOSE -> {
                when (step) {
                    LukaPaymentStep.BEGIN -> nativeChooseBegin(request)
                    LukaPaymentStep.PREPARE -> paymentPrepare(request, payload as SdkResult)
                    LukaPaymentStep.CHARGE -> anonymousPaymentCharge(request, payload as LukaTransaction)
                    else -> throw RuntimeException("Unknown action ${request.mAction}::$step for bridge {$javaClass}")
                }
            }
        }

    }

    override fun setup(api: LukaApi, config: LukaConfig, session: LukaSession): LiveData<Boolean> {
        mApi = api
        mSession = session
        mSetupResult = MutableLiveData()

        //Object to renew expired tokens
        mTokenSwitcher = BluesnapTokenSwitcher(mApi, mSession) {
            mToken = it
        }

        //Initialize SDK after first token retrieved
        mTokenSwitcher.mTokenHolder.once {
            BlueSnapService.getInstance()
                .setup(it, tokenProvider(), api.mContext, serviceCallback())
        }

        return mSetupResult
    }

    private fun anonymousPaymentBegin(request: LukaPaymentRequest): LiveData<LukaBridgeResponse> {
        val params = request.mParams

        val sdkRequest = LukaSdkRequest(
            params.amount,
            params.currency.iso,
            params.email == null,
            params.customButtonText
        )

        sdkRequest.isGooglePayTestMode = mApi.mTestMode
        sdkRequest.isActivate3DS = params.enable3dSecureAuthentication
        sdkRequest.isAllowCurrencyChange = false
        sdkRequest.isHideStoreCardSwitch = true


        BlueSnapService.getInstance().sdkRequest = sdkRequest

        return request.dispatch()
    }

    private fun cardPaymentBegin(request: LukaPaymentRequest): LiveData<LukaBridgeResponse> {
        val params = request.mParams as LukaCardPaymentParams
        val card = params.card

        val transaction = LukaTransaction(
            cardOwnerEmail = request.mParams.email ?: params.email,
            traceId = params.customTraceId ?: request.mSession.id,
            currency = params.currency,
            amount = params.amount,
            creditCardId = card.id,
            creditCard = card,
            creditCardOwner = CardOwner(
                id = params.lukaCustomerId,
                name = "",
                lastName = "",
            ),
        )

        return LiveDatas.from(LukaBridgeResponse(LukaPaymentStep.CHARGE, transaction))
    }

    private fun nativeChooseBegin(request: LukaPaymentRequest): LiveData<LukaBridgeResponse> {
        val params = request.mParams

        val sdkRequest = SdkRequestShopperRequirements(
            false,
            params.email == null,
            false
        )

        BlueSnapService.getInstance().sdkRequest = sdkRequest

        return request.dispatch()
    }

    private fun paymentPrepare(request: LukaPaymentRequest, payload: SdkResult): LiveData<LukaBridgeResponse> {
        val customerId : String? = if (request.mParams is LukaCardVaultParams) {
            request.mParams.lukaCustomerId
        } else {
            null
        }

        val addedCard: com.bluesnap.androidapi.models.CreditCard?
        val storeCard: Boolean

         if (request.mParams is LukaCardVaultParams) {
             val shopper = BlueSnapService.getInstance()
                 .getsDKConfiguration()
                 ?.shopper

             storeCard = true

             addedCard = shopper
                ?.newCreditCardInfo
                ?.creditCard
        } else {
             storeCard = false
             addedCard = null
         }

        val expDate = payload.expDate ?: addedCard?.let {
            val month = "%02d".format(it.expirationMonth)
            val year = "%04d".format(it.expirationYear)
            "$month/$year"
        }

        val transaction = LukaTransaction(
            cardOwnerEmail = request.mParams.email ?: payload.billingContactInfo.email,
            traceId = request.mParams.customTraceId ?: request.mSession.id,
            currency = LukaCurrency.valueOf(payload.currencyNameCode!!),
            amount = payload.amount,
            ref = "",
            creditCard = CreditCard(
                last4 = payload.last4Digits,
                expiresAt = expDate,
                countryCode = payload.billingContactInfo.country,
                type = payload.cardType,
                category = "CONSUMER",
                subType = "CREDIT",
            ),
            creditCardOwner = CardOwner(
                id = customerId ?: "",
                name = payload.billingContactInfo.firstName,
                lastName = payload.billingContactInfo.lastName,
            ),
            cardValidation = storeCard,
            bluesnapToken = payload.token
        )

        return LiveDatas.from(LukaBridgeResponse(LukaPaymentStep.CHARGE, transaction))
    }

    private fun anonymousPaymentCharge(request: LukaPaymentRequest, transaction: LukaTransaction): LiveData<LukaBridgeResponse> {
        return mApi.chargeTransaction(request.mSession, transaction)
            .onSuccess {
                if (it.isSuccessful) {
                    LukaBridgeResponse(
                        LukaPaymentStep.FINISH_SUCCESS,
                        LukaPaymentResult(it)
                    )
                } else {
                    LukaBridgeResponse(
                        LukaPaymentStep.FINISH_ERROR,
                        PaymentFailedException(LukaError.TRANSACTION_REJECTED, it.description)
                    )
                }
            }
            .onError {
                if (it is LukaHttpException) {

                    //TODO map LukaPay responses

                    val message = it.description
                        ?: "LukaPay responded with status = ${it.error.responseCode}"

                    val exception = when (message) {
                        "la tarjeta ya se encuentra registrada" ->
                            CardAlreadyRegisteredException(LukaError.TRANSACTION_REJECTED)
                        else ->
                            PaymentFailedException(LukaError.TRANSACTION_REJECTED, message)
                    }

                    LukaBridgeResponse(
                        LukaPaymentStep.FINISH_ERROR,
                        exception
                    )
                } else {
                    //failed to connect
                    LukaBridgeResponse(
                        LukaPaymentStep.FINISH_ERROR,
                        it
                    )
                }


            }
            .chain()
    }
}
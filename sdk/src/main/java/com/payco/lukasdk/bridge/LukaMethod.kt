/*
 * Copyright (c) 2021. Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 9/9/21 9:40 a. m.
 */

package com.payco.lukasdk.bridge

import com.payco.lukasdk.LukaCurrency

/**
 * Effective Payment methods
 */
enum class LukaMethod {

    /**
     * Credit card payment method
     */
    CreditCard {
        override fun gateway(): LukaGateway {
            return LukaGateway.Bluesnap
        }

        override fun supportedCurrencies(): Array<LukaCurrency> {
            return arrayOf(LukaCurrency.USD)
        }
    };

    /**
     * Gateway to process the payment method with.
     */
    internal abstract fun gateway(): LukaGateway

    /**
     * Retrieves the supported currencies for the payment method
     */
    abstract fun supportedCurrencies(): Array<LukaCurrency>
}
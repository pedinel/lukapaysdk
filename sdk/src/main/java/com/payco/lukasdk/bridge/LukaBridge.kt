/*
 * Copyright (c) 2021. Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 31/8/21 2:22 p. m.
 */

package com.payco.lukasdk.bridge

import androidx.annotation.CheckResult
import androidx.lifecycle.LiveData
import com.payco.lukasdk.LukaApi
import com.payco.lukasdk.services.auth.LukaSession
import com.payco.lukasdk.services.luka.LukaConfig
import com.payco.lukasdk.transaction.LukaPaymentRequest

/**
 * Communicates Luka with third party gateways
 */
internal interface LukaBridge {

    /**
     * Initializes Gateway
     */
    @CheckResult
    fun setup(api: LukaApi, config: LukaConfig, session: LukaSession): LiveData<Boolean>

    /**
     * Launches a single payment action
     */
    @CheckResult
    fun launch(step: LukaPaymentStep, request: LukaPaymentRequest, payload: Any?): LiveData<LukaBridgeResponse>
}
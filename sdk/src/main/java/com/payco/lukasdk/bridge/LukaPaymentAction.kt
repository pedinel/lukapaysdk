/*
 * Copyright (c) 2021. Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 30/9/21 2:09 p. m.
 */

package com.payco.lukasdk.bridge

internal enum class LukaPaymentAction {
    ANONYMOUS_CHARGE,
    CARD_STORAGE,
    NATIVE_CHOOSE,
    CARD_SELECTION_CHARGE
}
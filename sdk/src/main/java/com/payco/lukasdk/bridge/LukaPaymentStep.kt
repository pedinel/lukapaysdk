/*
 * Copyright (c) 2021. Soluciones Informáticas Manzanares - All Rights Reserved.
 * Unauthorized copying of this file, via any medium is strictly prohibited,
 * proprietary and confidential.
 * Written by Pedro Parra <pedrop@manzanares.com.ve>.
 * Last Modified 9/9/21 9:40 a. m.
 */

package com.payco.lukasdk.bridge

/**
 * Current payment step
 */
enum class LukaPaymentStep {
    /**
     * Setup payment before start
     */
    SETUP,

    /**
     * The payment process is starting
     */
    BEGIN,

    /**
     * The payment is being prepared
     */
    PREPARE,

    /**
     * The payment is being charged
     */
    CHARGE,

    /**
     * The payment was completed successfully
     */
    FINISH_SUCCESS,

    /**
     * The payment process was aborted due to an error
     */
    FINISH_ERROR
}
# About
LukaPay's Android SDK enables you to easily accept credit card payments directly from your Android app, and then process payments from your server using the Payment API. When you use this library, LukaPay handles most of the PCI compliance burden for you, as the shopper's payment data is tokenized and sent directly to LukaPay's servers.

# Versions
This SDK supports Android SDK 30 and above for development. The minimum Android API version for applications is 21, See  [Android devices](https://developer.android.com/about/dashboards/index.html) for device coverage.

# Installation

Step 1:
```
allprojects {
	repositories {
	    maven { url 'https://jitpack.io' }
	}
}
```

Step 2:
```
dependencies {
	implementation 'com.gitlab.pedinel:lukapaysdk:v1.1.7
}
```

# Available flows for collecting payment details

There are 2 supported flow types:
* Checkout: Create a transaction for a new or existing shopper
* Pay with selected payment method: Create a transaction using an existing shopper's selected payment method

# Usage

## Initialize

There are 2 supported initialization flows, if `LukaApp` interface is implemented or not.

### Implementing LukaApp Interface

Implement the LukaApp interface in your Application class.

Kotlin example:
```
import android.app.Application
import com.payco.lukasdk.LukaApiBuilder
import com.payco.lukasdk.LukaApp
import com.payco.lukasdk.services.auth.LukaAuthCredentials

class MyApplication : Application(), LukaApp {
    override fun getLukaCredentials(): LukaAuthCredentials {
        //Specify your LukaPay credentials
        return LukaAuthCredentials(
            user = "ridery",
            password = "short-rope-northern-quickly"
        )

    }

    override fun onLukaBuild(builder: LukaApiBuilder) {
        //Setup api mode
        builder.useTestingApi(true)
    }
    ...
}
```

Java example:
```
import android.app.Application;

import com.payco.lukasdk.LukaApiBuilder;
import com.payco.lukasdk.LukaApp;
import com.payco.lukasdk.services.auth.LukaAuthCredentials;

import androidx.annotation.NonNull;

class MyApplication extends Application implements LukaApp {
    @NonNull
    @Override
    public LukaAuthCredentials getLukaCredentials() {
        //Specify your LukaPay credentials
        return new LukaAuthCredentials(
                "ridery",
                "short-rope-northern-quickly"
        );
    }

    @Override
    public void onLukaBuild(@NonNull LukaApiBuilder builder) {
        //Setup api mode
        builder.useTestingApi(true);
    }
    ...
}
```

Note that in `onLukaBuild` method, you can set `builder.useTestingApi(true)` for testing purposes.

When `LukaApp` interface is implemented, LukaPay's SDK can be easily initialized by calling `initialize`, lets see some examples:

Kotlin:
```
import android.app.Application
import com.payco.lukasdk.Luka

class Application : Application(), LukaApp {
    override fun onCreate() {
        super.onCreate()
        
        Luka.initialize(this)
    }
    ...
}
```

Java:
```
import android.app.Application;

class ApplicationJava extends Application implements LukaApp {
    @Override
    public void onCreate() {
        super.onCreate();

        Luka.initialize(this);
    }
    ...
}
```

### Not-implementing LukaApp interface. This can be called in the `onCreate` of your Application

The LukaPay's SDK must be initialized before the `setup`.

Kotlin:
```
//Alternative to initialize SDK without implementing LukaApp interface
val credentials = LukaAuthCredentials(
    user = "ridery",
    password = "short-rope-northern-quickly"
)

Luka.initialize(applicationContext, credentials) { builder ->
    builder.useTestingApi(true)
}
```

Java:
```
LukaAuthCredentials credentials = new LukaAuthCredentials(
    "ridery",
    "short-rope-northern-quickly"
);

Luka.initialize(this, credentials, builder -> {
    builder.useTestingApi(true);
    return null;
});
```

## Setup

This prepares the Activity or Fragment to handle callbacks for LukaPay's SDK. The `setup` must be done in the `onCreate`. 

Kotlin example:
```
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        ...

        //This must be called in onCreate of your payment activity or fragment
        Luka.setup(this)
    }
    ...
}
```

Java example:
```
class MainActivityJava extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        ...

        //This must be called in onCreate of your payment activity or fragment
        Luka.setup(this);
    }
    ...
}
```

## Checkout

Launches an anonymous payment flow.

### Payment Parameters

Set up the payment method, amount and currency

Kotlin:
```
val params = LukaPaymentParams(
    LukaMethod.CreditCard,
    97.52,
    LukaCurrency.USD,
    "my-custom-traceId"//optional
)
```

Java:
```
LukaPaymentParams params = new LukaPaymentParams(
    LukaMethod.CreditCard,
    97.52,
    LukaCurrency.USD,
    "my-custom-traceId"//optional
);
```

### Payment callbacks and execution

Defines what to do in any of the execution results. The process is called asynchorously

Kotlin:
```
Luka.api()
    .createPaymentRequest(params)
    .onSuccess {
        mText.text = "Payment Successful! ${it.currency.symbol}${it.amount}"
        //Payment was charged successfully
        Log.i("LukaPaySDK", "Payment success $it")
        mButton.isEnabled = true
    }
    .onProgress {
        mText.text = "Payment Progress: ${it.action}"
        if (it.action == LukaPaymentStep.SETUP) {
            mButton.isEnabled = false
            Log.i("LukaPaySDK", "Payment process setup started.")
            //Payment process was launched.
            //You can display the loading bar, lock controls.
            //The attribute it.traceId is null in this case (and only in this case).
        } else if (it.action == LukaPaymentStep.BEGIN) {
            //Payment process setup was successful. The payment process is starting.

            Log.i("LukaPaySDK", "Payment process begins. TraceId = ${it.traceId}")
            val traceId = it.traceId!!

            it.handler.pause()

            //store traceId somewhere safe. If something happens, you can recover the
            //transaction status with the traceId

            it.handler.resume()
        } else {
            //Other cases
            Log.i("LukaPaySDK", "Payment process process ${it.action}. TraceId = ${it.traceId}")
        }

    }
    .onError {
        //The payment process failed
        Log.w("LukaPaySDK", "Payment failed $it")
        mText.text = "Payment Failed! ${it}"
        mButton.isEnabled = true

        if (it is TransactionSetupFailedException) {
            //LukaPay failed to initialize. Maybe wrong credentials supplied or
            //connection issues
        } else if (it is PaymentFailedException) {
            //Payment failed.
            val error = it.error
            val description = it.message
        } else {
            //Non-SDK related errors. Maybe connection timeout or parsing errors
        }


    }
    .execute(this)//launches the payment flow
```

Java:
```
Luka.api()
    .createPaymentRequest(params)
    .onSuccess(payment -> {
        String amount = payment.getCurrency().getSymbol() + payment.getAmount();
        String message = "Payment Successful! " + amount;
        //Payment was charged successfully
        Log.i("LukaPaySDK", "Payment success" + payment.toString());
        mButton.setEnabled(true);
        return null;
    }) 
    .onProgress(progress -> {
        String message = "Payment Progress: " + progress.getAction();
        
        if (it.action == LukaPaymentStep.SETUP) {
            mButton.setEnabled(false);
            Log.i("LukaPaySDK", "Payment process setup started.");
            //Payment process was launched.
            //You can display the loading bar, lock controls.
            //The attribute it.traceId is null in this case (and only in this case).
        } else if (it.action == LukaPaymentStep.BEGIN) {
            //Payment process setup was successful. The payment process is starting.

            String traceId = progress.getTraceId();
            Log.i("LukaPaySDK", "Payment process begins. TraceId = " + traceId);

            //pause flow
            progress.handler.pause();

            //store traceId somewhere safe. If something happens, you can recover the
            //transaction status with the traceId
            
            //resume flow
            progress.handler.resume();
        } else {
            //Other cases
            String action = progress.getAction();
            Log.i("LukaPaySDK", "Payment process process " + action + ". TraceId = " + progress.getTraceId());
        }
        return null;
    })
    .onError(e -> {
        //The payment process failed
        Log.w("LukaPaySDK", "Payment failed " + e.toString());
        mText.text = "Payment Failed! " + e.toString();
        mButton.setEnabled(true);

        if (e instanceOf TransactionSetupFailedException) {
            //LukaPay failed to initialize. Maybe wrong credentials supplied or
            //connection issues
        } else if (e instanceOf PaymentFailedException) {
            //Payment failed.
            LukaError error = e.getError();
            String description = e.getMessage();
        } else {
            //Non-SDK related errors. Maybe connection timeout or parsing errors
        }
        return null;

    })
    .execute(this);//launches the payment flow
```

## Check Transaction State

The transaction state can be checked if something went wrong in middle of the payment process. In the Luka payment flow, the `traceId` can be stored in the `LukaPaymentStep.BEGIN` progress step. That `traceId` is used to recover the payment process and check if the payment was charged or not.

Kotlin:
```
val traceId = "2802afa2-e684-48af-8a59-edbb694b8d1b"
Luka.api()
    .checkTransaction(traceId)
    .onSuccess {
        //Payment found
        Log.i("TEST", "Transaction found: $it charged: ${it.charged}")
    }
    .onError {
        //The check process failed
        Log.w("TEST", "Transaction failed to fetch $it")

        if (it is PaymentNotFoundException) {
            //No payment found with specified traceId
        }
    }
    .execute(this)
```

Java:
```
String traceId = "2802afa2-e684-48af-8a59-edbb694b8d1b";
Luka.api()
    .checkTransaction(traceId)
    .onSuccess(payment -> {
        //Payment found
        return null;
    })
    .onError(e -> {
        //The check process failed
        Log.w("TEST", "Transaction failed to fetch" + e.toString());

        if (e instanceOf PaymentNotFoundException) {
            //No payment found with specified traceId
        }
        return null;
    }
    .execute(this);
```

## Store a new card for customer (vault). 1$ will be charged for this operation.

Kotlin:
```
String lukaCustomerId = "61c652a1-5a9d-4341-a18b-a4526298827e"
String email = "customerEmail@example.com"

Luka.api()
    .addCustomerCardRequest(email, lukaCustomerId)
    .onSuccess {
        mText.text = "Stored Successfully! ${it.currency.symbol}"
        //Payment was charged successfully
        Log.i("TEST", "Stored $it")
        mButton.isEnabled = true
    }
    .onProgress {
        //update loaders or progress bars
    }
    .onError {
        //The payment process failed
        Log.w("TEST", "Store failed $it")
        mText.text = "Store Failed! ${it}"
        mButton.isEnabled = true
    }
    .execute(this)
```

## Index cards for customer (vault)

Kotlin:
```
String lukaCustomerId = "61c652a1-5a9d-4341-a18b-a4526298827e"
Luka.api()
    .indexCustomerCardsRequest(lukaCustomerId)
    .onSuccess {
        mText.text = "Cards list: ${it.size}"

        //you can choose a card by its id and call

        mButton.isEnabled = true
        
        val firstCard = it.cards[0]
    }
    .onError {
        //failed
        Log.w("TEST", "Fetch customer cards failed $it")
        mText.text = "Fetch Failed! ${it}"
        mButton.isEnabled = true
    }
    .execute(this)
```

## Pay using a vault credit card
This method requires a CreditCard object returned in the "Index cards for customer (vault)" flow.

Kotlin:

```
String lukaCustomerId = "61c652a1-5a9d-4341-a18b-a4526298827e"
String email = "customerEmail@example.com"

val params = LukaCardPaymentParams(
    lukaPayerId,
    creditCard,//returned in "Index cards for customer" flow
    97.52,
    LukaCurrency.USD,
    email,
    "my-custom-traceId"//optional
)

Luka.api()
    .createPaymentRequest(params)
    .onSuccess {
        //same than Anonymous payment flow
    }
    .onProgress {
        //same than Anonymous payment flow
    }
    .onError {
        //same than Anonymous payment flow
    }
    .execute(this)
```


## Delete card from vault
This method requires a CreditCard id returned in the "Index cards for customer (vault)" flow.

Kotlin:

```
String lukaCustomerId = "61c652a1-5a9d-4341-a18b-a4526298827e"
String lukaCardId = "11223344-5a9d-4341-a18b-aa5566998877"

Luka.api()
    .deleteCustomerCardsRequest(lukaCustomerId, lukaCardId)
    .onSuccess {
        //card deleted
    }
    .onError {
        //couldnt delete card
    }
    .execute(this)
```
